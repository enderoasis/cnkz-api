<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use TelegramNotifications\TelegramChannel;
use TelegramNotifications\Messages\TelegramMessage;


class TelegramNotification extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($task,$assignee,$owner_name)
    {
        $this->task = $task;
        $this->assignee = $assignee;
        $this->owner_name = $owner_name;

    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return [TelegramChannel::class];
    }


    public function toTelegram()
    {
        $url = env('FRONT_URL') . '/tasks/' . $this->task->id;

        return (new TelegramMessage())->text('<b>Кому назначено: </b>' . $this->assignee->surname . $this->assignee->name . PHP_EOL . '<b>Кто назначил: </b>' . $this->owner_name . PHP_EOL . '<b>Задача: </b>' . $this->task->title . PHP_EOL . '<b>На сайте: </b>' . $url 
        )->parse_mode('html');

    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
