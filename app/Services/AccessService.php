<?php

namespace App\Services;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Permission;
use App\Models\Role;
use Illuminate\Support\Facades\Log;
use Validator;

class AccessService 
{	
	public function all()
	{
        try {
            $user = auth()->userOrFail();
        } catch (\Tymon\JWTAuth\Exceptions\UserNotDefinedException $e) {
            //Log::channel('telegram')->error($e->getMessage());
            return response()->json(['error' => 'Unauthorized'], 401);
        }   
        $array['roles'] = Role::all();
        $array['permissions'] = Permission::select('id','name')->get();
        
        return response()->json(['success' => true, 'response' => $array], 201);
    }

	public function create()
    {
        try {
            $user = auth()->userOrFail();
        } catch (\Tymon\JWTAuth\Exceptions\UserNotDefinedException $e) {
            //Log::channel('telegram')->error($e->getMessage());
            return response()->json(['error' => 'Unauthorized'], 401);
        }   

        try
        {
            $array = [];
            $data = $this->request->get('data');

            // $city = City::firstOrNew(['name' =>  $data['name']]);
            // $success = true;
            // $array['city'] = $city;

            // return response()->json(['success' => $success, 'response' => $array], 201);

        }
        catch(\Exception $e)
        {
            Log::channel('telegram')->error($e->getMessage());
            return response()->json(['success' => false, 'response' => $e->getMessage()], 400);
        }
    }

    public function update($id)
    {
        try {
            $user = auth()->userOrFail();
        } catch (\Tymon\JWTAuth\Exceptions\UserNotDefinedException $e) {
            //Log::channel('telegram')->error($e->getMessage());
            return response()->json(['error' => 'Unauthorized'], 401);
        }   

        // $city = City::where('id', $id)->first();
        // $array = [];
        // $data = $this->request->get('data');

        // if ($city == null)
        // {
        //     return response()->json(['success' => false, 'response' => 'Город не найден'], 404);
        // }
        // else{
        //         $city->name = $data['name'];
        //         $city->update();
            
        //       return response()->json(['success' => true, 'response' => 'Город обновлен'], 201);
        // }

    }

    public function request($request)
    {
        $this->request = $request;
        return $this;
    }
}