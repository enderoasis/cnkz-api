<?php

namespace App\Services;

use Illuminate\Http\Request;
use App\Models\Income;
use App\Models\Expense;
use App\Models\Analytic;
use Illuminate\Support\Facades\Log;
use Validator;

class StatService 
{	
	public function all()
	{
        try {
            $user = auth()->userOrFail();
        } catch (\Tymon\JWTAuth\Exceptions\UserNotDefinedException $e) {
            //Log::channel('telegram')->error($e->getMessage());
            return response()->json(['error' => 'Unauthorized'], 401);
        }
        if($user->can('analytic-view'))
        {
            $chart_data = Analytic::getChartsData(request('analytic_period_type') ?? 1, request('city_id'));
            return $chart_data;
        }
	}

	public function request($request)
    {
        $this->request = $request;
        return $this;
    }
}