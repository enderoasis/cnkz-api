<?php

namespace App\Services;

use Illuminate\Http\Request;
use App\Models\Task;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;
use Validator;
use Illuminate\Support\Facades\Input;
use App\Models\User;
use App\Mail\TaskAssigned;
use App\Notifications\TelegramNotification;

class TaskService 
{	
	public function all()
	{
       
        $success = false;
        $array = [];

        $tasks = Task::getCurrentUserTasks(request('start_date'), request('end_date'));

        foreach($tasks as $task)
        {
            $t_raw = [
                'id' => $task['id'],
                'assignee_id' => $task['assignee_id'],
                'assignee_name' => User::find($task['assignee_id'])->surname . ' ' . User::find($task['assignee_id'])->name,
                'start' => $task['start_date'],
                'status_id' => $task['status_id'],
                'status_name' => \DB::table('task_statuses')->where('id',$task['status_id'])->first()->name,
                'status_color' => \DB::table('task_statuses')->where('id',$task['status_id'])->first()->status_color,
                'end' => $task['end_date'],
                'owner_id' => $task['owner_id'],
                'owner_name' => User::find($task['owner_id'])->surname . ' ' . User::find($task['owner_id'])->name,
                'title' => $task['title'],
                'description' => $task['description']
            ];

            array_push($array,$t_raw);
        }

        return response()->json(['success' => true, 'response' => $array], 200);

	}

	public function create()
    {
        try {
            $user = auth()->userOrFail();
        } catch (\Tymon\JWTAuth\Exceptions\UserNotDefinedException $e) {
            //Log::channel('telegram')->error($e->getMessage());
            return response()->json(['error' => 'Unauthorized'], 401);
        }   

		$success = false;
        $data = $this->request->get('data');
        $array = [];

        $validator = Validator::make($this->request->get('data'), [
            'assignee_id' => 'required',
            'start_date' => 'required',
            'end_date' => 'required',
            'title' => 'required',
        ])->passes();

        if (!$validator) {
            return response()->json(['success' => $success, 'response' => $validator->messages()], 400);
        }

        try
        {
            $start_date = Carbon::parse($data['start_date'])->format('Y-m-d H:i:s');
            $end_date = Carbon::parse($data['end_date'])->format('Y-m-d H:i:s');

            
            $task = new Task;
            $task->assignee_id = $data['assignee_id'];
            $task->owner_id = auth()->user()->id;
            $task->start_date = $start_date;
            $task->end_date = $end_date;
            $task->status_id = 1;
            $task->title = $data['title'];
            $task->description = $data['description'] ?? null;
            $task->save();

            $success = true;
            $array['task'] = [
                'id' => $task['id'],
                'assignee_id' => $task['assignee_id'],
                'assignee_name' => User::find($task['assignee_id'])->surname . ' ' . User::find($task['assignee_id'])->name,
                'start' => $task['start_date'],
                'status_id' => 1 , // Новая
                'status_name' => \DB::table('task_statuses')->where('id',$task['status_id'])->first()->name,
                'status_color' => \DB::table('task_statuses')->where('id',$task['status_id'])->first()->status_color,
                'end' => $task['end_date'],
                'owner_id' => $task['owner_id'],
                'owner_name' => User::find($task['owner_id'])->surname . ' ' . User::find($task['owner_id'])->name,
                'title' => $task['title'],
                'description' => $task['description']
            ];

            if($task->assignee_id)
            {
                $assignee = User::find($task->assignee_id);
                // Mail notify
                \Mail::to($assignee->email)->send(new TaskAssigned($assignee, $array['task']));
                // Telegram notify
                $task->notify(new TelegramNotification($task,$assignee,$array['task']['owner_name']));
            }

            return response()->json(['success' => $success, 'response' => $array], 201);

        }
        catch(\Exception $e)
        {
            Log::channel('telegram')->error($e->getMessage());
            return response()->json(['success' => false, 'response' => $e->getMessage()], 400);
        }
	}

    public static function latest()
    {
        try {
            $user = auth()->userOrFail();
        } catch (\Tymon\JWTAuth\Exceptions\UserNotDefinedException $e) {
            //Log::channel('telegram')->error($e->getMessage());
            return response()->json(['error' => 'Unauthorized'], 401);
        }   

        $array = array();
        $tasks = Task::where('assignee_id',  $user->id)->whereDate('created_at', Carbon::today())->select('id','owner_id','title','created_at')->get();

        foreach($tasks as $task)
        {
            $t_raw = [
                'id' => $task['id'],
                'owner_name' => User::find($task['owner_id'])->surname . ' ' . User::find($task['owner_id'])->name,
                'title' => $task['title'],
                'h_dif' => $task['created_at']->diffInHours(Carbon::now())
            ];

            array_push($array,$t_raw);
        }
        return $array;
    }
	public function one($id)
    {
        try {
            $user = auth()->userOrFail();
        } catch (\Tymon\JWTAuth\Exceptions\UserNotDefinedException $e) {
            //Log::channel('telegram')->error($e->getMessage());
            return response()->json(['error' => 'Unauthorized'], 401);
        }   

		$id = $this->request->get('id') ?? $id;
        $array = [];

        $task = Task::where('id', $id)->first();
        
        $resp = [
            'id' => $task['id'],
            'assignee_id' => $task['assignee_id'],
            'assignee_name' => User::find($task['assignee_id'])->surname . ' ' . User::find($task['assignee_id'])->name,
            'start' => $task['start_date'],
            'status_id' => $task['status_id'],
            'status_name' => \DB::table('task_statuses')->where('id',$task['status_id'])->first()->name,
            'status_color' => \DB::table('task_statuses')->where('id',$task['status_id'])->first()->status_color,
            'end' => $task['end_date'],
            'owner_id' => $task['owner_id'],
            'owner_name' => User::find($task['owner_id'])->surname . ' ' . User::find($task['owner_id'])->name,
            'title' => $task['title'],
            'description' => $task['description']
        ];
        $success = true;
        
        if($task == null)
        {
            return response()->json(['success' => false, 'response' => 'Задача не найдена'], 404);
        }
        else{
            return response()->json(['success' => $success, 'response' => $resp], 200);
        }
	}

	public function update($id)
    {
        try {
            $user = auth()->userOrFail();
        } catch (\Tymon\JWTAuth\Exceptions\UserNotDefinedException $e) {
            //Log::channel('telegram')->error($e->getMessage());
            return response()->json(['error' => 'Unauthorized'], 401);
        }   

        $task = Task::find($id);
        $data = $this->request->get('data');
        $array = [];

        if ($task == null)
        {
            return response()->json(['success' => false, 'response' => 'Задача не найдена'], 404);
        }
        try{
            
            $task->status_id = $data['status_id'] ?? $task->status_id;
            $task->assignee_id = $data['assignee_id']  ??  $task->assignee_id;
            $task->start_date = Carbon::parse($data['start_date']  ??  $task->start_date)->format('Y-m-d H:i:s');
            $task->end_date = Carbon::parse($data['end_date']  ?? $task->end_date)->format('Y-m-d H:i:s');
            $task->title = $data['title']  ??  $task->title;
            $task->description = $data['description']  ??  $task->description;         
            $task->update();
            
			$array['task'] = [
                'id' => $task['id'],
                'assignee_id' => $task['assignee_id'],
                'assignee_name' => User::find($task['assignee_id'])->surname . ' ' . User::find($task['assignee_id'])->name,
                'start' => $task['start_date'],
                'status_id' => $task['status_id'],
                'status_name' => \DB::table('task_statuses')->where('id',$task['status_id'])->first()->name,
                'status_color' => \DB::table('task_statuses')->where('id',$task['status_id'])->first()->status_color,
                'end' => $task['end_date'],
                'owner_id' => $task['owner_id'],
                'owner_name' => User::find($task['owner_id'])->surname . ' ' . User::find($task['owner_id'])->name,
                'title' => $task['title'],
                'description' => $task['description']
            ];

        }
        catch(\Exception $e)
        {
            Log::channel('telegram')->error($e->getMessage());
        }

        return response()->json(['success' => true, 'response' => $array], 201);

    }

    public function delete($id)
    {
        try {
            $user = auth()->userOrFail();
        } catch (\Tymon\JWTAuth\Exceptions\UserNotDefinedException $e) {
            //Log::channel('telegram')->error($e->getMessage());
            return response()->json(['error' => 'Unauthorized'], 401);
        }   
        
        $task = Task::where('id', $id)->first();
        $array = [];

        if ($task == null)
        {
            return response()->json(['success' => false, 'response' => 'Задача не найдена'], 404);
        }
        else{
              $task->delete();

              return response()->json(['success' => true, 'response' => 'Задача удалена'], 201);
        }

    }
	public function request($request)
    {
        $this->request = $request;
        return $this;
    }
}