<?php

namespace App\Services;

use Illuminate\Http\Request;
use App\Models\Income;
use App\Models\Contract;
use Illuminate\Support\Facades\Log;
use Validator;
use App\Models\File;

class IncomeService 
{	
	public function all()
	{
        try {
            $user = auth()->userOrFail();
        } catch (\Tymon\JWTAuth\Exceptions\UserNotDefinedException $e) {
            //Log::channel('telegram')->error($e->getMessage());
            return response()->json(['error' => 'Unauthorized'], 401);
        }   

        $array = [];
        // TO DO by client_id etc
        if($user->can('contract-view-all'))
        {
            $incomes =  Income::orderBy('date','DESC')->get();
        }
        else{
            $incomes =  Income::leftJoin('contracts', 'contracts.id', '=', 'incomes.contract_id')
            ->where('contracts.city_id',$user->city_id)
            ->select('incomes.id as id',
                'incomes.type_id as type_id',
                'incomes.contract_id as contract_id',
                'incomes.date as date',
                'incomes.name as name',
                'incomes.value as value',
                'incomes.purpose as purpose'
            )
            ->orderBy('date','DESC')
            ->get();
        }
        
        foreach($incomes as $income)
        {
            $t_raw = [
                'id' => $income['id'],
                'type_id' => $income['type_id'],
                'type_name' => \DB::table('income_types')->where('id', $income['type_id'])->first()->name,
                'contract' => Contract::find($income['contract_id']),
                'date' => $income['date'],
                'name' => $income['name'],
                'value' => $income['value'],
                'purpose' => $income['purpose']
            ];

            array_push($array,$t_raw);
        }

        return response()->json(['success' => true, 'response' => $array], 200);

	}

	public function create()
    {

        try {
            $user = auth()->userOrFail();
        } catch (\Tymon\JWTAuth\Exceptions\UserNotDefinedException $e) {
            //Log::channel('telegram')->error($e->getMessage());
            return response()->json(['error' => 'Unauthorized'], 401);
        }   

		$success = false;
        $data = $this->request->get('data');
        $array = [];

        $validator = Validator::make($this->request->get('data'), [
            'type_id' => 'required',
            'contract_id' => 'required',
            'value' => 'required',
			'date' => 'required',
        ]);

        if (!$validator) {
            return response()->json(['success' => $success, 'response' => $validator->messages()], 400);
        }
        try
        {
            $income = new Income;
            $income->type_id = $data['type_id'];
            $income->contract_id = $data['contract_id'];
            $income->name = $data['name'];
            $income->date = $data['date'];
            $income->value = $data['value'];
            $income->purpose = $data['purpose'];
            $income->save();

            $success = true;
            $t_raw = [
                'id' => $income['id'],
                'type_id' => $income['type_id'],
                'type_name' => \DB::table('income_types')->where('id', $income['type_id'])->first()->name,
                'contract' => Contract::find($income['contract_id'])->first(),
                'date' => $income['date'],
                'name' => $income['name'],
                'value' => $income['value'],
                'purpose' => $income['purpose']
            ];

            return response()->json(['success' => $success, 'response' => $t_raw], 201);

        }
        catch(\Exception $e)
        {
            Log::channel('telegram')->error($e->getMessage());
            return response()->json(['success' => false, 'response' => $e->getMessage()], 400);
        }


	}

	public function one($id)
    {
        try {
            $user = auth()->userOrFail();
        } catch (\Tymon\JWTAuth\Exceptions\UserNotDefinedException $e) {
            //Log::channel('telegram')->error($e->getMessage());
            return response()->json(['error' => 'Unauthorized'], 401);
        }   

		$id = $this->request->get('id') ?? $id;
        $array = [];

        $income = Income::where('id', $id)->first();
        $array['income'] = $income;
        $success = true;
        
        if($income == null)
        {
            return response()->json(['success' => false, 'response' => 'Запись о доходе не найдена'], 404);
        }
        else{

            $response = [
                'id' => $income['id'],
                'type_id' => $income['type_id'],
                'type_name' => \DB::table('income_types')->where('id', $income['type_id'])->first()->name,
                'contract_id' => $income['contract_id'],
                'date' => $income['date'],
                'name' => $income['name'],
                'value' => $income['value'],
                'purpose' => $income['purpose'],
                'files' => File::where('model_type','Income')->where('model_id', $income->id)->select('id','name')->get()
            ];

            return response()->json(['success' => $success, 'response' => $response], 200);
        }
	}

	public function update($id)
    {
        try {
            $user = auth()->userOrFail();
        } catch (\Tymon\JWTAuth\Exceptions\UserNotDefinedException $e) {
            //Log::channel('telegram')->error($e->getMessage());
            return response()->json(['error' => 'Unauthorized'], 401);
        }   

        $income = Income::where('id', $id)->first();
        $data = $this->request->get('data');
        $array = [];

        if ($income == null)
        {
            return response()->json(['success' => false, 'response' => 'Запись о доходе не найдена'], 404);
        }
        try{
            $income->type_id = $data['type_id'] ?? $income->type_id;
            $income->contract_id = $data['contract_id'] ?? $income->contract_id;
            $income->name = $data['name'] ?? $income->name;
            $income->date = $data['date'] ?? $income->date;
            $income->value = $data['value'] ?? $income->value;
            $income->purpose = $data['purpose'] ?? $income->purpose;
            $income->update();

			$array['income'] = $income;

            return response()->json(['success' => true, 'response' => $array], 201);

        }
        catch(\Exception $e)
        {
            Log::channel('telegram')->error($e->getMessage());
            return response()->json(['success' => false, 'response' => $e->getMessage()], 400);
        }
    }

    public function delete($id)
    {
        try {
            $user = auth()->userOrFail();
        } catch (\Tymon\JWTAuth\Exceptions\UserNotDefinedException $e) {
            //Log::channel('telegram')->error($e->getMessage());
            return response()->json(['error' => 'Unauthorized'], 401);
        }   
        
        $income = Income::where('id', $id)->first();

        if ($income == null)
        {
            return response()->json(['success' => false, 'response' => 'Запись о доходе не найдена'], 404);
        }
        else{
              $income->delete();

              return response()->json(['success' => true, 'response' => 'Запись о доходе удалена'], 201);
        }

    }
	public function request($request)
    {
        $this->request = $request;
        return $this;
    }
}