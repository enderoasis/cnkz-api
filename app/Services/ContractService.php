<?php

namespace App\Services;

use Illuminate\Http\Request;
use App\Models\Contract;
use Illuminate\Support\Facades\Log;
use App\Models\ContractType;
use App\Models\ContractStatus;
use App\Models\City;
use App\Models\Client;
use App\Models\File;
use Carbon\Carbon;
use Validator;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Symfony\Component\HttpFoundation\StreamedResponse;

class ContractService 
{	
	public function all()
	{
        try {
            $user = auth()->userOrFail();
        } catch (\Tymon\JWTAuth\Exceptions\UserNotDefinedException $e) {
            //Log::channel('telegram')->error($e->getMessage());
            return response()->json(['error' => 'Unauthorized'], 401);
        }   

        $contracts = Contract::getCurrentUserContracts();
        $array = [];
        $array['contracts'] = $contracts ?? [];
        $array['cities'] = City::all();
        $array['clients'] = \DB::table("clients")->select("id", \DB::raw("CONCAT(surname, ' ', name) as name"))->get();
        $array['contract_types'] = ContractType::all();
        $array['contract_statuses'] = ContractStatus::all();

        return response()->json(['success' => true, 'response' => $array], 200);
	}

	public function create()
    {
        try {
            $user = auth()->userOrFail();
        } catch (\Tymon\JWTAuth\Exceptions\UserNotDefinedException $e) {
            //Log::channel('telegram')->error($e->getMessage());
            return response()->json(['error' => 'Unauthorized'], 401);
        }       
             if($user->can('contract-edit'))
        {
            $success = false;
            $data = $this->request->get('data');
            $array = [];
    
            $validator = Validator::make($this->request->get('data'), [
                'type_id' => 'required',
                'city_id' => 'required',
                'client_id' => 'required',
                'status_id' => 'required',
                'contract_number' => 'required',
                'quadrature' => 'required',
                'start_date' => 'required',
                'end_date' => 'required'
            ]);
    
            if (!$validator) {
                return response()->json(['success' => $success, 'response' => $validator->messages()], 400);
            }
            try
            {
                $user = auth()->user();
    
                $contract = new Contract;
                $contract->type_id = $data['type_id'];
                $contract->client_id = $data['client_id'];
                $contract->status_id = $data['status_id'];
                $contract->contract_number = $data['contract_number'];
                $contract->quadrature = $data['quadrature'];

                $notify_date = Carbon::createFromFormat('Y-m-d', $data['end_date'], 'Asia/Almaty');   

                $contract->notify_date = $notify_date->subWeek();

                if($user->can('contract-view-city') && $user->hasRole('manager'))
                {
                    $contract->city_id = $user->city_id;
                }
                else{
                    $contract->city_id = $data['city_id'];
                }
                $contract->start_date = $data['start_date'];
                $contract->end_date = $data['end_date'];
                $contract->save();
    
                $success = true;
                $array['contract'] = [
                    'id' => $contract->id,
                    'type_name' => ContractType::find($contract->type_id)->name,
                    'client_name' => Client::find($contract->client_id)->select(\DB::raw("CONCAT(surname, ' ', name) as name"))->first()->name,
                    'status_name' => ContractStatus::find($contract->status_id)->name,
                    'contract_number' => $contract->contract_number,
                    'quadrature' => $contract->quadrature,
                    'city_name' => City::find($contract->city_id)->name,
                    'notify_date' => $contract->notify_date
                ];

                return response()->json(['success' => $success, 'response' => $array], 201);

            }
            catch(\Exception $e)
            {
                Log::channel('telegram')->error($e->getMessage());
                return response()->json(['success' => false, 'response' => $e->getMessage()], 400);
            }
        }
        else {
            return response()->json(['success' => false, 'response' => "No access create contract"], 403);
        }
	}

	public function one($id)
    {
        try {
            $user = auth()->userOrFail();
        } catch (\Tymon\JWTAuth\Exceptions\UserNotDefinedException $e) {
            //Log::channel('telegram')->error($e->getMessage());
            return response()->json(['error' => 'Unauthorized'], 401);
        }   

		$id = $this->request->get('id') ?? $id;
        $array = [];

        $contract = Contract::where('id', $id)->first();
        $response = [
                'id' => $contract->id,
                'type_name' => ContractType::find($contract->type_id)->name,
                'status_name' => ContractStatus::find($contract->status_id)->name,
                'contract_number' => $contract->contract_number,
                'quadrature' => $contract->quadrature,
                'city_name' => City::find($contract->city_id)->name,
                'notify_date' => $contract->notify_date,
                'start_date' =>  $contract->start_date,
                'end_date' =>  $contract->end_date,
                'status_color' => ContractStatus::find($contract->status_id)->status_color,
                'client' => [
                    'client_name' => $contract->client->surname . ' ' . $contract->client->name,
                    'email' => $contract->client->email,
                    'phone' => $contract->client->phone,
                    'company_name' => $contract->client->company_name,
                    'city_name' => City::find($contract->client->city_id)->name
                ],
                'files' => File::where('model_type','Contract')->where('model_id', $contract->id)->select('id','name')->get()
            ];

            $success = true;
            
        if($contract == null)
        {
            Log::channel('telegram')->info("Договор не найден, id: " . $id);

            return response()->json(['success' => false, 'response' => 'Договор не найден'], 404);
        }
        else{
            return response()->json(['success' => $success, 'response' => $response], 200);
        }
      
	}

	public function update($id)
    {

        try {
            $user = auth()->userOrFail();
        } catch (\Tymon\JWTAuth\Exceptions\UserNotDefinedException $e) {
            //Log::channel('telegram')->error($e->getMessage());
            return response()->json(['error' => 'Unauthorized'], 401);
        }   

        $contract = Contract::where('id', $id)->first();
        $data = $this->request->get('data');
        $array = [];

        if($user->can('contract-edit'))
        {
            if ($contract == null)
            {
                Log::channel('telegram')->info("Договор не найден, id: " . $id);

                return response()->json(['success' => false, 'response' => 'Договор не найден'], 404);
            }
            try{
            
                $contract->type_id = $data['type_id'] ?? $contract->type_id;
                $contract->client_id = $data['client_id']  ??  $contract->client_id;
                $contract->status_id = $data['status_id']  ??  $contract->status_id;
                $contract->contract_number = $data['contract_number']  ?? $contract->contract_number;
                $contract->quadrature = $data['quadrature']  ??  $contract->quadrature;
                $contract->city_id = $data['city_id']  ??  $contract->city_id;
                $contract->start_date = $data['start_date']  ??  $contract->start_date;
                $contract->end_date = $data['end_date']  ??  $contract->end_date;
                $contract->notify_date = $data['notify_date']  ??  $contract->notify_date;
                $contract->update();

                $array['contract'] = [
                    'id' => $contract->id,
                    'type_name' => ContractType::find($contract->type_id)->name,
                    'client_name' => \DB::table('clients')->where('id', $contract->client_id)->select(\DB::raw("CONCAT(surname, ' ', name) as name"))->first()->name,
                    'status_name' => ContractStatus::find($contract->status_id)->name,
                    'contract_number' => $contract->contract_number,
                    'quadrature' => $contract->quadrature,
                    'type_id' => $contract->type_id,
                    'status_id' => $contract->status_id,
                    'city_name' => City::find($contract->city_id)->name,
                    'notify_date' => $contract->notify_date
                ];

                return response()->json(['success' => true, 'response' => $array], 201);

            }
            catch(\Exception $e)
            {
                Log::channel('telegram')->error($e->getMessage());
                return response()->json(['success' => false, 'response' => $e->getMessage()], 400);
            }
        }
        else{
            return response()->json(['success' => false, 'response' => "No write access for contracts"], 403);
        }
    }

    public function delete($id)
    {

        try {
            $user = auth()->userOrFail();
        } catch (\Tymon\JWTAuth\Exceptions\UserNotDefinedException $e) {
            //Log::channel('telegram')->error($e->getMessage());
            return response()->json(['error' => 'Unauthorized'], 401);
        }   
        
        $contract = Contract::where('id', $id)->first();
        $array = [];

        if ($contract == null)
        {
            Log::channel('telegram')->info("Договор не найден, id: " . $id);

            return response()->json(['success' => false, 'response' => 'Договор не найден'], 404);
        }
        else{
              $contract->delete();

              return response()->json(['success' => true, 'response' => 'Договор удален'], 201);
        }

    }

    public function getClientContracts($id)
    {
        try {
            $user = auth()->userOrFail();
        } catch (\Tymon\JWTAuth\Exceptions\UserNotDefinedException $e) {
            //Log::channel('telegram')->error($e->getMessage());
            return response()->json(['error' => 'Unauthorized'], 401);
        }   

        $contracts = Contract::leftJoin('cities', 'cities.id', '=', 'contracts.city_id')
        ->leftJoin('contract_types', 'contract_types.id', '=', 'contracts.type_id')
        ->leftJoin('contract_statuses', 'contract_statuses.id', '=', 'contracts.status_id')
        ->leftJoin('clients', 'clients.id', '=', 'contracts.client_id')
        ->where('contracts.client_id',$id)
        ->select('contracts.id as id',
            'contracts.quadrature as quadrature',
            'contracts.contract_number as contract_number',
            \DB::raw('CONCAT(clients.surname, " ", clients.name) AS client_name'),
            'contract_types.name as type_name',
            'cities.name as city_name',
            'contract_statuses.name as status_name',
            'contract_statuses.status_color as status_color',
            'contracts.start_date as start_date',
            'contracts.end_date as end_date',
            'contracts.notify_date as notify_date'
        )
        ->get();

        return $contracts;
    }

    public function getExcel()
    {
        try {
            $user = auth()->userOrFail();
        } catch (\Tymon\JWTAuth\Exceptions\UserNotDefinedException $e) {
            //Log::channel('telegram')->error($e->getMessage());
            return response()->json(['error' => 'Unauthorized'], 401);
        }   

        $spreadsheet = new Spreadsheet();

        $contracts = Contract::leftJoin('cities', 'cities.id', '=', 'contracts.city_id')
        ->leftJoin('contract_types', 'contract_types.id', '=', 'contracts.type_id')
        ->leftJoin('contract_statuses', 'contract_statuses.id', '=', 'contracts.status_id')
        ->leftJoin('clients', 'clients.id', '=', 'contracts.client_id')
        ->select('contracts.id as id',
            'contracts.quadrature as quadrature',
            'contracts.contract_number as contract_number',
            'clients.surname as client_surname',
            'clients.name as client_name',
            'contract_types.name as type_name',
            'cities.name as city_name',
            'contract_statuses.name as status_name',
            'contracts.start_date as start_date',
            'contracts.end_date as end_date',
            'contracts.notify_date as notify_date'
        )
        ->get();

        $spreadsheet->getActiveSheet()->setTitle('Contracts Report');
        $header = ['Номер договора', 'Квадратура', 'Фамилия', 'Имя', 'Тип',
            'Город', 'Статус', 'Начало', 'Конец'];

        $spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(20);
        $spreadsheet->getActiveSheet()->getColumnDimension('B')->setWidth(10);
        $spreadsheet->getActiveSheet()->getColumnDimension('C')->setWidth(30);
        $spreadsheet->getActiveSheet()->getColumnDimension('D')->setWidth(20);
        $spreadsheet->getActiveSheet()->getColumnDimension('E')->setWidth(20);
        $spreadsheet->getActiveSheet()->getColumnDimension('F')->setWidth(10);
        $spreadsheet->getActiveSheet()->getColumnDimension('G')->setWidth(15);
        $spreadsheet->getActiveSheet()->getColumnDimension('H')->setWidth(15);
        $spreadsheet->getActiveSheet()->getColumnDimension('I')->setWidth(15);

        $spreadsheet->getDefaultStyle()->getFont()->setName('Arial');
        $spreadsheet->getDefaultStyle()->getFont()->setSize(12);
        $spreadsheet->getDefaultStyle()->getFont()->getColor();
        $sheet = $spreadsheet->getActiveSheet();

        $sheet->fromArray($header, null, 'A1');
        $row_line = 1;

        foreach ($contracts as $data) {
            $row_line++;
            $sheet->setCellValue('A' . $row_line, $data['contract_number']);
            $sheet->setCellValue('B' . $row_line, $data['quadrature']);
            $sheet->setCellValue('C' . $row_line, $data['client_surname']);
            $sheet->setCellValue('D' . $row_line, $data['client_name']);
            $sheet->setCellValue('E' . $row_line, $data['type_name']);
            $sheet->setCellValue('F' . $row_line, $data['city_name']);
            $sheet->setCellValue('G' . $row_line, $data['status_name']);
            $sheet->setCellValue('H' . $row_line, $data['start_date']);
            $sheet->setCellValue('I' . $row_line, $data['end_date']);
        }

        $writer = new Xlsx($spreadsheet);

        $response = new StreamedResponse(
            function () use ($writer) {
                $writer->save('php://output');
            }
        );
        $response->headers->set('Content-Type', 'application/vnd.ms-excel');
        $response->headers->set('Content-Disposition', 'attachment;filename=' . 'ContractsReport - '. Carbon::today() . '.xlsx' . '');
        $response->headers->set('Cache-Control', 'max-age=0');

        return $response;
    }
	public function request($request)
    {
        $this->request = $request;
        return $this;
    }
}