<?php

namespace App\Services;

use Illuminate\Http\Request;
use App\Models\Income;
use App\Models\Expense;
use App\Models\Client;
use App\Models\File;
use App\Models\Contract;
use Illuminate\Support\Facades\Log;
use Validator;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Carbon\Carbon;
use PDF;

class FinanceReportService 
{	
	public function all()
	{
        try {
            $user = auth()->userOrFail();
        } catch (\Tymon\JWTAuth\Exceptions\UserNotDefinedException $e) {
            //Log::channel('telegram')->error($e->getMessage());
            return response()->json(['error' => 'Unauthorized'], 401);
        }   

        if($user->can('report-view'))
        {
            // if admin
            if($user->hasRole('admin'))
            {
                $contract_ids = Contract::select('id')->pluck('id');
            }
            // If user is manager
            if($user->hasRole('manager'))
            {
                $contract_ids = Contract::select('id')->where('city_id',$user->city_id)->pluck('id');
            }
            // if client
            if($user->hasRole('client'))
            {
                $client = Client::where('email',$user->email)->first();
                $contract_ids = $client->contracts->pluck('id'); 
            }
          
            $incomes = Income::whereIn('contract_id',$contract_ids)->get();
            $expenses = Expense::whereIn('contract_id',$contract_ids)->get();

            $income_arr = [];
            $expense_arr = [];

            foreach($incomes as $income)
            {
                $income_raw = [
                    'date' => $income['date'],
                    'value' => $income['value'],
                    'type_name' => 'Поступление',
                    'purpose' => $income['purpose'],
                    'comission' => '1%',
                    'files' => File::where('model_type','Income')->where('model_id', $income['id'])->select('id','name')->get()
                ];
    
                array_push($income_arr,$income_raw);
            }

            foreach($expenses as $expense)
            {
                $expense_raw = [
                    'date' => $expense['date'],
                    'value' => $expense['value'],
                    'type_name' => 'Расход',
                    'purpose' => $expense['purpose'],
                    'comission' => null,
                    'files' => File::where('model_type','Expense')->where('model_id', $expense['id'])->select('id','name')->get()
                ];
    
                array_push($expense_arr,$expense_raw);
            }

            $reports_raw = array_merge($income_arr,$expense_arr);

            $collection = collect($reports_raw)->sortByDesc('date');
            
            return $collection->values()->all();
        }
    }

    public function one($id)
    {

    }

    public function getExcel()
    {
        try {
            $user = auth()->userOrFail();
        } catch (\Tymon\JWTAuth\Exceptions\UserNotDefinedException $e) {
            //Log::channel('telegram')->error($e->getMessage());
            return response()->json(['error' => 'Unauthorized'], 401);
        }   

        $spreadsheet = new Spreadsheet();

        $contracts = $this->all();

        
        $spreadsheet->getActiveSheet()->setTitle('Financial Report');
        $header = ['Дата', 'Сумма', 'Тип', 'Назначение', 'Коммисия'];

        $spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(10);
        $spreadsheet->getActiveSheet()->getColumnDimension('B')->setWidth(10);
        $spreadsheet->getActiveSheet()->getColumnDimension('C')->setWidth(30);
        $spreadsheet->getActiveSheet()->getColumnDimension('D')->setWidth(30);
        $spreadsheet->getActiveSheet()->getColumnDimension('E')->setWidth(20);

        $spreadsheet->getDefaultStyle()->getFont()->setName('Arial');
        $spreadsheet->getDefaultStyle()->getFont()->setSize(12);
        $spreadsheet->getDefaultStyle()->getFont()->getColor();
        
        $sheet = $spreadsheet->getActiveSheet();

        $sheet->fromArray($header, null, 'A1');
        $row_line = 1;

        foreach ($contracts as $data) {
            $row_line++;
            $sheet->setCellValue('A' . $row_line, $data['date']);
            $sheet->setCellValue('B' . $row_line, $data['value']);
            $sheet->setCellValue('C' . $row_line, $data['type_name']);
            $sheet->setCellValue('D' . $row_line, $data['purpose']);
            $sheet->setCellValue('E' . $row_line, $data['comission']);
        }

        $writer = new Xlsx($spreadsheet);

        $response = new StreamedResponse(
            function () use ($writer) {
                $writer->save('php://output');
            }
        );
        $response->headers->set('Content-Type', 'application/vnd.ms-excel');
        $response->headers->set('Content-Disposition', 'attachment;filename=' . 'FinancialReport - '. Carbon::today() . '.xlsx' . '');
        $response->headers->set('Cache-Control', 'max-age=0');

        return $response;
    }

    public function getPdf()
    {
        $reports = $this->all();
        $pdf = PDF::loadView('pdf.report', ['reports' => $reports]);
     
        return $pdf->download('Report.pdf');
    
    }

    public function dateSort($a,$b)
    {
        $dateA = strtotime($a['date']);
        $dateB = strtotime($b['date']);
        return ($dateA-$dateB);
    }
	public function request($request)
    {
        $this->request = $request;
        return $this;
    }
}