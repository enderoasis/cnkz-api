<?php

namespace App\Services;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\City;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Log;
use App\Mail\UserRegistered;
use Validator;

class UserService 
{	
     public function all()
     {
        $users = User::all();

        $array = [];

        foreach($users as $user)
        {
            $roles_arr = [];
            foreach($user->roles as $role)
            {
                array_push($roles_arr,$role['name']);
            }
            $response = [
                'id' => $user['id'],
                'name' => $user['name'],
                'surname' => $user['surname'],
                'email' => $user['email'],
                'city_name' => City::find($user['city_id'])->name,
                'role' => $roles_arr
            ];

            array_push($array,$response);
        }


        return response()->json(['success' => true, 'response' => $array], 200);

     }
	 public function create($data)
     {
        try
        {
            if($data == null)
            {
                $data = $this->request->get('data');
            }

            $validator = Validator::make($data, [
                'name' => 'required',
                'surname' => 'required',
                'email' => 'required|string|email:rfc,dns|max:50|unique:users',
                'city_id' => 'required'
            ]);

            if ($validator->fails()) {
                return response()->json(['success' => false, 'response' => $validator->errors()], 400);
            }

            $tmp_password = Str::random(5);

            $user = new User;
            $user->email = $data['email'];
            $user->name = $data['name'];
            $user->surname = $data['surname'];
            $user->password = bcrypt($tmp_password);
            $user->city_id = $data['city_id'];
            $user->save();

            // Role assign
            \DB::table('users_roles')->upsert([
                [
                    'user_id' => $user->id,
                    'role_id' => $data['role_id'] ?? 3
                ]
                ], ['user_id','role_id'],['role_id']);
            
            if($data['role_id'])
            {
                $permissions = \DB::table('roles_permissions')->where('role_id', $data['role_id'])->pluck('permission_id');
                foreach($permissions as $permission)
                {
                    \DB::table('users_permissions')->upsert([
                        [
                            'user_id' => $user->id,
                            'permission_id' => $permission
                        ]
                        ], ['user_id','permission_id'],['permission_id']);
                }
            }

            Log::channel('telegram')->info("New user created: " . PHP_EOL . $user);

            // Send notification to email
            \Mail::to($data['email'])->send(new UserRegistered($user,$tmp_password));

            return response()->json(['success' => true, 'response' => $user], 201);

        }
        catch(\Exception $e)
        {
            Log::channel('telegram')->error($e->getMessage());
            return response()->json(['success' => false, 'response' => $e->getMessage()], 400);

        }
     }
     public function update($id)
     {
         $user = User::where('id',$id)->first();
         $data = $this->request->get('data');

         if($user == null)
         {
            return response()->json(['success' => false, 'response' => "User not found id: " . $id], 404);
         }
         else{
            try
            {
                $user->email = $data['email'];
                $user->surname = $data['surname'];
                $user->name = $data['name'];
                $user->city_id = $data['city_id'];
                $user->update();
    
                if($data['role_id'])
                {
                // Role update
                    \DB::table('users_roles')->upsert([
                        [
                            'user_id' => $user->id,
                            'role_id' => $data['role_id'] ?? 3
                        ]
                        ], ['user_id','role_id'],['role_id']);
                }

                if($data['permissions'])
                {
                // Permission update
                    \DB::table('users_permissions')->where('user_id',$user->id)->delete();
                    
                    foreach($data['permissions'] as $permission)
                    {
                        \DB::table('users_permissions')->upsert([
                            [
                                'user_id' => $user->id,
                                'permission_id' => $permission
                            ]
                            ], ['user_id','permission_id'],['permission_id']);
                    }
                }

                Log::channel('telegram')->info("User edited: " . PHP_EOL . $id);
    
                return response()->json(['success' => true, 'response' => $user], 201);
            }
            catch(\Exception $e)
            {
                Log::channel('telegram')->error($e->getMessage());
                return response()->json(['success' => false, 'response' => $e->getMessage()], 400);
            }
         }
     }

     public function one($id)
     {
         $user = User::where('id',$id)->first();

         if($user == null)
         {
            return response()->json(['success' => false, 'response' => "User not found id: " . $id], 404);

         }
         else{
            $response = [
                'id' => $user->id,
                'name' => $user->name,
                'surname' => $user->surname,
                'email' => $user->email,
                'city_id' => $user->city_id,
                'role_id' => \DB::table('users_roles')->where('user_id',$user->id)->select('role_id')->first()->role_id ?? 3,
                'permissions' => \DB::table('users_permissions')->where('user_id', $user->id)->pluck('permission_id')
            ];
            return response()->json(['success' => true, 'response' => $response], 200);
         }
     }

     public function delete($id)
     {
        $user = User::where('id',$id)->first();

        if($user == null)
         {
            return response()->json(['success' => false, 'response' => "User not found id: " . $id], 404);
         }
         else{
            try
            {
                if(auth()->userOrFail()->can('user-delete'))
                {
                    User::removeFromSystem($user->id);

                    return response()->json(['success' => true, 'response' => "User deleted id: " . $id], 200);
                }
                else{
                    return response()->json(['success' => false, 'response' => "No access for deleting user"], 403);
                }
            }
            catch(\Exception $e)
            {
                Log::channel('telegram')->error($e->getMessage());
                return response()->json(['success' => false, 'response' => $e->getMessage()], 400);
            }
         }
     }
     public function request($request)
    {
        $this->request = $request;
        return $this;
    }

}