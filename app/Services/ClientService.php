<?php

namespace App\Services;

use Illuminate\Http\Request;
use App\Models\Client;
use App\Models\Contract;
use App\Models\City;
use App\Models\User;
use App\Services\UserService;
use Illuminate\Support\Facades\Log;
use Validator;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Carbon\Carbon;

class ClientService 
{	
	public function all()
	{
        try {
            $user = auth()->userOrFail();
        } catch (\Tymon\JWTAuth\Exceptions\UserNotDefinedException $e) {
           // Log::channel('telegram')->error($e->getMessage());
           return response()->json(['error' => 'Unauthorized'], 401);
        }

        if($user->can('client-view-all'))
        {
            $clients = Client::leftJoin('cities', 'cities.id', '=', 'clients.city_id')
            ->select('clients.id as id',
                'clients.company_name as company_name',
                'clients.name as name',
                'clients.surname as surname',
                'clients.birth_date as birth_date',
                'clients.phone as phone',
                'clients.email as email',
                'cities.name as city_name',
                'clients.created_at as created_at'
            )
            ->get();

            return $clients;
        }
        if($user->can('client-view-city'))
        {
            $clients = Client::leftJoin('cities', 'cities.id', '=', 'clients.city_id')
            ->where('clients.city_id',$user->city_id)
            ->select('clients.id as id',
                'clients.company_name as company_name',
                'clients.name as name',
                'clients.surname as surname',
                'clients.birth_date as birth_date',
                'clients.phone as phone',
                'clients.email as email',
                'cities.name as city_name',
                'clients.created_at as created_at'
            )
            ->get();

            return $clients;
        }
	}

    public function create()
    {
        try {
            $user = auth()->userOrFail();
        } catch (\Tymon\JWTAuth\Exceptions\UserNotDefinedException $e) {
            Log::channel('telegram')->error($e->getMessage());
            return response()->json(['error' => 'Unauthorized'], 401);
        }
        if($user->can('client-edit'))
        {
            $array = [];
            $success = false;
            $data = $this->request->get('data');
    
            
            $validator = Validator::make($this->request->get('data'), [
                'name' => 'required',
                'surname' => 'required',
                'phone' => 'required|unique:clients',
                'email' => 'required|string|email|max:100|unique:users',
                'city_id' => 'required'
            ]);
    
            if ($validator->fails()) {
                return response()->json(['success' => false, 'response' => $validator->errors()], 400);
            }

            try
            {
                $client = new Client;
                $client->company_name = $data['company_name'];
                $client->name = $data['name'];
                $client->surname = $data['surname'];
                $client->birth_date = $data['birth_date'];
                $client->phone = $data['phone'];
                $client->city_id = $data['city_id'];
                $client->email = $data['email'];
                $client->save();
    
                $success = true;
                $array['client'] = [
                    'id' => $client->id,
                    'company_name' => $client->company_name,
                    'name' => $client->name,
                    'surname' => $client->surname,
                    'birth_date' => $client->birth_date,
                    'phone' => $client->phone,
                    'city_name' => City::find($client->city_id)->name,
                    'email' => $client->email,
                    'created_at' => $client->created_at
                ];

                $data['role_id'] = 3;
                UserService::create($data);
                
                return response()->json(['success' => $success, 'response' => $array], 201);

            }
            catch(\Exception $e)
            {
                Log::channel('telegram')->error($e->getMessage());
                return response()->json(['success' => $success, 'response' => $e->getMessage()], 400);

            }
            // Create user with client data
    
        }
        else{
            return response()->json(['success' => false, 'response' => 'No permission client edit'], 403);
        }
    }

    public function one($id)
    {
        try {
            $user = auth()->userOrFail();
        } catch (\Tymon\JWTAuth\Exceptions\UserNotDefinedException $e) {
            //Log::channel('telegram')->error($e->getMessage());
            return response()->json(['error' => 'Unauthorized'], 401);
        }   
        if($user->can('client-view-all'))
        {
            $id = $this->request->get('id') ?? $id;
            $array = [];
    
            $client = Client::where('id',$id)->first();
            
            $response = [
                'id' => $client->id,
                'company_name' => $client->company_name,
                'name' => $client->name,
                'surname' => $client->surname,
                'birth_date' => $client->birth_date,
                'phone' => $client->phone,
                'city_name' => City::find($client->city_id)->name,
                'email' => $client->email,
                'created_at' => $client->created_at,
            ];

            $success = true;
            
            if($client == null)
            {
                return response()->json(['success' => false, 'response' => 'Клиент не найден'], 404);
            }
            else{
                return response()->json(['success' => $success, 'response' => $response], 200);
            }
        }
        else{
            return response()->json(['success' => false, 'response' => 'No permission client view'], 403);
        }
	}

    public function update($id)
    {
        try {
            $user = auth()->userOrFail();
        } catch (\Tymon\JWTAuth\Exceptions\UserNotDefinedException $e) {
            //Log::channel('telegram')->error($e->getMessage());
            return response()->json(['error' => 'Unauthorized'], 401);
        }
        
        if($user->can('client-edit'))
        {
            $client = Client::where('id',$id)->first();
            $data = $this->request->get('data');
            $array = [];
            if ($client == null)
            {
                return response()->json(['success' => false, 'response' => 'Клиент не найден'], 404);
            }
            try{
                $client->company_name = $data['company_name'] ?? $client->company_name;
                $client->name = $data['name']  ??  $client->name;
                $client->surname = $data['surname']  ??  $client->surname;
                $client->birth_date = $data['birth_date']  ?? $client->birth_date;
                $client->phone = $data['phone']  ??  $client->phone;
                $client->city_id = $data['city_id']  ??  $client->city_id;
                $client->email = $data['email']  ??  $client->email;
                $client->update();
                
                $array['client'] = [
                    'id' => $client->id,
                    'company_name' => $client->company_name,
                    'name' => $client->name,
                    'surname' => $client->surname,
                    'birth_date' => $client->birth_date,
                    'phone' => $client->phone,
                    'city_name' => City::find($client->city_id)->name,
                    'email' => $client->email,
                    'created_at' => $client->created_at,
                ];

                return response()->json(['success' => true, 'response' => $array], 201);

            }
            catch(\Exception $e)
            {
                Log::channel('telegram')->error($e->getMessage());
                return response()->json(['success' => false, 'response' => $e->getMessage()], 400);

            }
        }
        else{
            return response()->json(['success' => false, 'response' => 'No permission client edit'], 403);
        }
    }

    public function delete($id)
    {
        try {
            $user = auth()->userOrFail();
        } catch (\Tymon\JWTAuth\Exceptions\UserNotDefinedException $e) {
            //Log::channel('telegram')->error($e->getMessage());
            return response()->json(['error' => 'Unauthorized'], 401);
        }   

        $client = Client::where('id', $id)->first();
        $array = [];

        if ($client == null)
        {
            Log::channel('telegram')->info("Клиент не найден, id: " . $id);

            return response()->json(['success' => false, 'response' => 'Клиент не найден'], 404);
        }
        else{
              $client->contracts()->delete();
              $client->delete();
              $client_user = User::where('email',$client['email'])->first();
              if($client_user)
              {
                User::removeFromSystem($client_user->id);
              }
              
              return response()->json(['success' => true, 'response' => 'Клиент удален'], 201);
        }
    }

    public function getExcel()
    {
        try {
            $user = auth()->userOrFail();
        } catch (\Tymon\JWTAuth\Exceptions\UserNotDefinedException $e) {
            //Log::channel('telegram')->error($e->getMessage());
            return response()->json(['error' => 'Unauthorized'], 401);
        }   

        $spreadsheet = new Spreadsheet();

        $clients = Client::leftJoin('cities', 'cities.id', '=', 'clients.city_id')
        ->select('clients.id as id',
            'clients.surname as client_surname',
            'clients.name as client_name',
            'cities.name as city_name',
            'clients.birth_date as birth_date',
            'clients.phone as phone',
            'clients.email as email'
        )
        ->get();

        $spreadsheet->getActiveSheet()->setTitle('Clients Report');
        $header = ['Идентификатор', 'Фамилия', 'Имя', 'Город', 'Дата рождения',
            'Телефон', 'Почта'];

        $spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(15);
        $spreadsheet->getActiveSheet()->getColumnDimension('B')->setWidth(25);
        $spreadsheet->getActiveSheet()->getColumnDimension('C')->setWidth(15);
        $spreadsheet->getActiveSheet()->getColumnDimension('D')->setWidth(15);
        $spreadsheet->getActiveSheet()->getColumnDimension('E')->setWidth(15);
        $spreadsheet->getActiveSheet()->getColumnDimension('F')->setWidth(20);
        $spreadsheet->getActiveSheet()->getColumnDimension('G')->setWidth(20);

        $spreadsheet->getDefaultStyle()->getFont()->setName('Arial');
        $spreadsheet->getDefaultStyle()->getFont()->setSize(12);
        $spreadsheet->getDefaultStyle()->getFont()->getColor();
        $sheet = $spreadsheet->getActiveSheet();

        $sheet->fromArray($header, null, 'A1');
        $row_line = 1;

        foreach ($clients as $data) {
            $row_line++;
            $sheet->setCellValue('A' . $row_line, $data['id']);
            $sheet->setCellValue('B' . $row_line, $data['client_surname']);
            $sheet->setCellValue('C' . $row_line, $data['client_name']);
            $sheet->setCellValue('D' . $row_line, $data['city_name']);
            $sheet->setCellValue('E' . $row_line, $data['birth_date']);
            $sheet->setCellValue('F' . $row_line, $data['phone']);
            $sheet->setCellValue('G' . $row_line, $data['email']);
        }

        $writer = new Xlsx($spreadsheet);

        $response = new StreamedResponse(
            function () use ($writer) {
                $writer->save('php://output');
            }
        );
        $response->headers->set('Content-Type', 'application/vnd.ms-excel');
        $response->headers->set('Content-Disposition', 'attachment;filename=' . 'ClientsReport - '. Carbon::today() . '.xlsx' . '');
        $response->headers->set('Cache-Control', 'max-age=0');

        return $response;
    }

    public function request($request)
    {
        $this->request = $request;
        return $this;
    }

}