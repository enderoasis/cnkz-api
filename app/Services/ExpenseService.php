<?php

namespace App\Services;

use Illuminate\Http\Request;
use App\Models\Expense;
use Illuminate\Support\Facades\Log;
use App\Models\Contract;
use App\Models\File;

class ExpenseService 
{	
	public function all()
	{
        try {
            $user = auth()->userOrFail();
        } catch (\Tymon\JWTAuth\Exceptions\UserNotDefinedException $e) {
            //Log::channel('telegram')->error($e->getMessage());
            return response()->json(['error' => 'Unauthorized'], 401);
        }   

        $array = [];
        // TO DO by client_id etc
        if($user->can('contract-view-all'))
        {
            $expenses =  Expense::orderBy('date','DESC')->get();
        }
        else{
            $expenses =  Expense::leftJoin('contracts', 'contracts.id', '=', 'expenses.contract_id')
            ->where('contracts.city_id',$user->city_id)
            ->select('expenses.id as id',
                'expenses.type_id as type_id',
                'expenses.contract_id as contract_id',
                'expenses.date as date',
                'expenses.name as name',
                'expenses.value as value',
                'expenses.purpose as purpose'
            )
            ->orderBy('date','DESC')
            ->get();
        }

        foreach($expenses as $expense)
        {
            $t_raw = [
                'id' => $expense['id'],
                'type_id' => $expense['type_id'],
                'type_name' => \DB::table('expense_types')->where('id', $expense['type_id'])->first()->name,
                'contract' => Contract::find($expense['contract_id']),
                'date' => $expense['date'],
                'name' => $expense['name'],
                'value' => $expense['value'],
                'purpose' => $expense['purpose']
            ];

            array_push($array,$t_raw);
        }

        return response()->json(['success' => true, 'response' => $array], 200);

	}

	public function create()
    {

        try {
            $user = auth()->userOrFail();
        } catch (\Tymon\JWTAuth\Exceptions\UserNotDefinedException $e) {
            //Log::channel('telegram')->error($e->getMessage());
            return response()->json(['error' => 'Unauthorized'], 401);
        }   

		$success = false;
        $data = $this->request->get('data');
        $array = [];

        $validator = \Validator::make($this->request->get('data'), [
            'type_id' => 'required',
            'contract_id' => 'required',
            'value' => 'required',
			'date' => 'required',
        ])->passes();

        if (!$validator) {
            return response()->json(['success' => $success, 'response' => $validator->messages()], 400);
        }
        try
        {
            $expense = new Expense;
            $expense->type_id = $data['type_id'];
            $expense->contract_id = $data['contract_id'];
            $expense->name = $data['name'];
            $expense->date = $data['date'];
            $expense->value = $data['value'];
            $expense->purpose = $data['purpose'];
            $expense->save();

            $success = true;
            $t_raw = [
                'id' => $expense['id'],
                'type_id' => $expense['type_id'],
                'type_name' => \DB::table('expense_types')->where('id', $expense['type_id'])->first()->name,
                'contract_id' => $expense['contract_id'],
                'date' => $expense['date'],
                'name' => $expense['name'],
                'value' => $expense['value'],
                'purpose' => $expense['purpose']
            ];

            return response()->json(['success' => $success, 'response' => $t_raw], 201);
        }
        catch(\Exception $e)
        {
            Log::channel('telegram')->error($e->getMessage());
            return response()->json(['success' => false, 'response' => $e->getMessage()], 400);

        }
	}

	public function one($id)
    {
        try {
            $user = auth()->userOrFail();
        } catch (\Tymon\JWTAuth\Exceptions\UserNotDefinedException $e) {
            //Log::channel('telegram')->error($e->getMessage());
            return response()->json(['error' => 'Unauthorized'], 401);
        }   

		$id = $this->request->get('id') ?? $id;
        $array = [];

        $expense = Expense::where('id', $id)->first();
        $success = true;
        
        if($expense == null)
        {
            return response()->json(['success' => false, 'response' => 'Запись о расходе не найдена'], 404);
        }
        else{
            $response = [
                'id' => $expense['id'],
                'type_id' => $expense['type_id'],
                'type_name' => \DB::table('expense_types')->where('id', $expense['type_id'])->first()->name,
                'contract_id' => $expense['contract_id'],
                'date' => $expense['date'],
                'name' => $expense['name'],
                'value' => $expense['value'],
                'purpose' => $expense['purpose'],
                'files' => File::where('model_type','Expense')->where('model_id', $expense['id'])->select('id','name')->get()
            ];

            return response()->json(['success' => $success, 'response' => $response], 200);
        }
	}

	public function update($id)
    {

        try {
            $user = auth()->userOrFail();
        } catch (\Tymon\JWTAuth\Exceptions\UserNotDefinedException $e) {
            //Log::channel('telegram')->error($e->getMessage());
            return response()->json(['error' => 'Unauthorized'], 401);
        }   

        $expense = Expense::where('id', $id)->first();
        $data = $this->request->get('data');
        $array = [];

        if ($expense == null)
        {
            return response()->json(['success' => false, 'response' => 'Запись о расходе не найдена'], 404);
        }
        try{
            $expense->type_id = $data['type_id'] ?? $expense->type_id;
            $expense->contract_id = $data['contract_id'] ?? $expense->contract_id;
            $expense->name = $data['name'] ?? $expense->name;
            $expense->date = $data['date'] ?? $expense->date;
            $expense->value = $data['value'] ?? $expense->value;
            $expense->purpose = $data['purpose'] ?? $expense->purpose;
            $expense->update();

			$array['expense'] = $expense;

            return response()->json(['success' => true, 'response' => $array], 201);

        }
        catch(\Exception $e)
        {
            Log::channel('telegram')->error($e->getMessage());
            return response()->json(['success' => false, 'response' => $e->getMessage()], 400);
        }
    }

    public function delete($id)
    {

        try {
            $user = auth()->userOrFail();
        } catch (\Tymon\JWTAuth\Exceptions\UserNotDefinedException $e) {
            //Log::channel('telegram')->error($e->getMessage());
            return response()->json(['error' => 'Unauthorized'], 401);
        }   
        
        $expense = Expense::where('id', $id)->first();
        $array = [];

        if ($expense == null)
        {
            return response()->json(['success' => false, 'response' => 'Запись о расходе не найдена'], 404);
        }
        else{
              $expense->delete();

              return response()->json(['success' => true, 'response' => 'Запись о расходе удалена'], 201);
        }

    }
	public function request($request)
    {
        $this->request = $request;
        return $this;
    }
}