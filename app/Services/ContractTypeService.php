<?php

namespace App\Services;

use Illuminate\Http\Request;
use App\Models\ContractType;
use Illuminate\Support\Facades\Log;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class ContractTypeService 
{	
	public function all()
	{
        try {
            $user = auth()->userOrFail();
        } catch (\Tymon\JWTAuth\Exceptions\UserNotDefinedException $e) {
            //Log::channel('telegram')->error($e->getMessage());
            return response()->json(['error' => 'Unauthorized'], 401);
        }   

        $contract_types = ContractType::all();
        $array = [];
        $array['contract_types'] = $contract_types;

        return response()->json(['success' => true, 'response' => $array], 200);
	}

	public function create()
    {
         try {
            $user = auth()->userOrFail();
        } catch (\Tymon\JWTAuth\Exceptions\UserNotDefinedException $e) {
            //Log::channel('telegram')->error($e->getMessage());
            return response()->json(['error' => 'Unauthorized'], 401);
        }   
              if($user->can('contract_type-edit'))
        {
            $success = false;
            $data = $this->request->get('data');
            $array = [];
    
            $validator = \Validator::make($this->request->get('data'), [
                'name' => 'required',
            ])->passes();
    
            if (!$validator) {
                return response()->json(['success' => $success, 'response' => $validator->messages()], 400);
            }
            try
            {
                $user = auth()->user();
    
                $contract_type = new ContractType;
                $contract_type->name = $data['name'];
                $contract_type->save();
    
                $success = true;
                $array['contract_type'] = $contract_type;

                return response()->json(['success' => $success, 'response' => $array], 201);
            }
            catch(\Exception $e)
            {
                Log::channel('telegram')->error($e->getMessage());
                return response()->json(['success' => false, 'response' => $e->getMessage()], 400);

            }    
        }
        else {
            return response()->json(['success' => false, 'response' => "No access create contract_type"], 400);
        }
	}

	public function one($id)
    {
        try {
            $user = auth()->userOrFail();
        } catch (\Tymon\JWTAuth\Exceptions\UserNotDefinedException $e) {
            //Log::channel('telegram')->error($e->getMessage());
            return response()->json(['error' => 'Unauthorized'], 401);
        }   

		$id = $this->request->get('id') ?? $id;
        $array = [];

        $contract_type = ContractType::where('id', $id)->first();
        $array['contract_type'] = $contract_type;
        $success = true;
        
        if($contract_type == null)
        {

            return response()->json(['success' => false, 'response' => 'Тип договора не найден'], 404);
        }
        else{
            return response()->json(['success' => $success, 'response' => $contract_type], 200);
        }
	}

	public function update($id)
    {
        try {
            $user = auth()->userOrFail();
        } catch (\Tymon\JWTAuth\Exceptions\UserNotDefinedException $e) {
            //Log::channel('telegram')->error($e->getMessage());
            return response()->json(['error' => 'Unauthorized'], 401);
        }   

        $contract_type = ContractType::where('id', $id)->first();
        $data = $this->request->get('data');
        $array = [];

        if ($contract_type == null)
        {

            return response()->json(['success' => false, 'response' => 'Тип договора не найден'], 404);
        }
        try{
            $contract_type->update([
                'name' => $data['name'],
            ]);
           
			$array['contract_type'] = $contract_type;

            return response()->json(['success' => true, 'response' => $array], 201);

        }
        catch(\Exception $e)
        {
            Log::channel('telegram')->error($e->getMessage());
            return response()->json(['success' => false, 'response' => $e->getMessage()], 400);

        }
    }

    public function delete($id)
    {
        try {
            $user = auth()->userOrFail();
        } catch (\Tymon\JWTAuth\Exceptions\UserNotDefinedException $e) {
            //Log::channel('telegram')->error($e->getMessage());
            return response()->json(['error' => 'Unauthorized'], 401);
        }   
        
        $contract_type = ContractType::where('id', $id)->first();
        $array = [];

        if ($contract_type == null)
        {
            Log::channel('telegram')->info("Тип договора не найден, id: " . $id);

            return response()->json(['success' => false, 'response' => 'Тип договора не найден'], 404);
        }
        else{
              $contract_type->delete();

              return response()->json(['success' => true, 'response' => 'Тип договора удален'], 201);
        }

    }
	public function request($request)
    {
        $this->request = $request;
        return $this;
    }
}