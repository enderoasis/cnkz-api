<?php

namespace App\Services;

use Illuminate\Http\Request;
use App\Models\File;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Validator;

class FileService 
{	
	public function getModelFiles()
	{
        try {
            $user = auth()->userOrFail();
        } catch (\Tymon\JWTAuth\Exceptions\UserNotDefinedException $e) {
            //Log::channel('telegram')->error($e->getMessage());
            return response()->json(['error' => 'Unauthorized'], 401);
        }   

        $success = true;
        $files = File::where('model_id', $this->request->get('model_id'))->where('model_type',$this->request->get('model_type'))->get();
        $array['files'] = $files;
        
        return response()->json(['success' => $success, 'response' => $array], 200);

    }

	public function create()
    {
        try {
            $user = auth()->userOrFail();
        } catch (\Tymon\JWTAuth\Exceptions\UserNotDefinedException $e) {
            //Log::channel('telegram')->error($e->getMessage());
            return response()->json(['error' => 'Unauthorized'], 401);
        }   
            $files = $this->request->file('files');

            if($files)
            {
               foreach($files as $file_raw)
               {
                    $extension = $file_raw->getClientOriginalExtension();
                    $data_filename = explode('.' . $extension, $file_raw->getClientOriginalName());
                    $filename = $data_filename[0] . '.' . $extension;
            
                    $file = new File;
                    $file->user_id = \Auth::user()->id;
                    $file->model_id = $this->request->model_id;
                    $file->model_type = $this->request->model;

                    $file->name = $filename;
                    $file->format = $extension;
                    $file->save();
            
                    \Storage::disk('uploads')->putFileAs(
                        $file->model_type . '_' . $file->model_id,
                        $file_raw,
                        $file_raw->getClientOriginalName()
                    );
               }
                     return response()->json(['success' => true, 'response' => 'Файлы добавлены'], 201);

            }
            else{
                return response()->json(['success' => false, 'response' => 'Нет файлов'], 400);
            }       
    }

	public function delete($id)
    {
        try {
            $user = auth()->userOrFail();
        } catch (\Tymon\JWTAuth\Exceptions\UserNotDefinedException $e) {
            //Log::channel('telegram')->error($e->getMessage());
            return response()->json(['error' => 'Unauthorized'], 401);
        }   

        $file = File::where('id', $id)->first();
        $array = [];

        if ($file == null)
        {
            return response()->json(['success' => false, 'response' => 'Файл не найден'], 404);
        }
        else{
              $file->delete();

              return response()->json(['success' => true, 'response' => 'Файл удален'], 201);
        }

    }

    public function one($id)
    {
        try {
            $user = auth()->userOrFail();
        } catch (\Tymon\JWTAuth\Exceptions\UserNotDefinedException $e) {
            //Log::channel('telegram')->error($e->getMessage());
            return response()->json(['error' => 'Unauthorized'], 401);
        }   
        
        \Invoker::permission(__METHOD__, $this->canView);

        $file = File::where('id', $id)
            ->first();

        // Check file
        if ($file) {
            $storage = storage_path('app') . '/uploads/files/' . $file->hash;
            $headers = array(
                "Content-Type: $file->mime",
            );

            return response()->download($storage, $file->name, $headers);
        }

        return response(['success' => false, 'response' => 'Файл не найден'], 404);

    }


    public function request($request)
    {
        $this->request = $request;
        return $this;
    }
}