<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class UserRegistered extends Mailable
{
    use Queueable, SerializesModels;

    public $user;
    public $tmp_password;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user,$tmp_password)
    {
        $this->user = $user;
        $this->tmp_password = $tmp_password;

    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mail.user-registered', [
            'user' => $this->user,
            'tmp_password' => $this->tmp_password,
        ]);
    }
}
