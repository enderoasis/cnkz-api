<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class TaskAssigned extends Mailable
{
    use Queueable, SerializesModels;

    public $assignee;
    public $task;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($assignee,$task)
    {
        $this->assignee = $assignee;
        $this->task = $task;

    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('task.task-assigned', [
            'assignee' => $this->assignee,
            'task' => $this->task,
        ]);
    }
}
