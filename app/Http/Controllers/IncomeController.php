<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\IncomeService;

class IncomeController extends Controller
{
    public function index()
    {
        return (new IncomeService)->all();    
    }

    public function store(Request $request)
    {
        return (new IncomeService)
            ->request($request)
            ->create();
    }

    public function show(Request $request, $id)
    {
        return (new IncomeService)
            ->request($request)
            ->one($id);
    }

    public function update(Request $request, $id)
    {
        return (new IncomeService)
        ->request($request)
        ->update($id);
    }

    public function destroy($id)
    {
        return (new IncomeService)
            ->delete($id);
    }
}
