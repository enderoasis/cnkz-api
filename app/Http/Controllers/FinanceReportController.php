<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\FinanceReportService;

class FinanceReportController extends Controller
{
    public function index()
    {
        return (new FinanceReportService)->all();    
    }

    public function show(Request $request, $id)
    {
        return (new FinanceReportService)
            ->request($request)
            ->one($id);
    }
    public function formExcel()
    {
        return (new FinanceReportService)
        ->getExcel();
    }

    public function formPdf()
    {
        return (new FinanceReportService)
        ->getPdf();
    }
}
