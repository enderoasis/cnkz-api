<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\TaskService;

class TaskController extends Controller
{
    public function index()
    {
        return (new TaskService)->all();    
    }

    public function store(Request $request)
    {
        return (new TaskService)
            ->request($request)
            ->create();
    }

    public function show(Request $request, $id)
    {
        return (new TaskService)
            ->request($request)
            ->one($id);
    }

    public function update(Request $request, $id)
    {
        return (new TaskService)
        ->request($request)
        ->update($id);
    }
    
    public function getActualNotifies()
    {
        return (new TaskService)->latest();    
    }

    public function destroy($id)
    {
        return (new TaskService)
            ->delete($id);
    }
}
