<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\ExpenseService;

class ExpenseController extends Controller
{
    public function index()
    {
        return (new ExpenseService)->all();    
    }

    public function store(Request $request)
    {
        return (new ExpenseService)
            ->request($request)
            ->create();
    }

    public function show(Request $request, $id)
    {
        return (new ExpenseService)
            ->request($request)
            ->one($id);
    }

    public function update(Request $request, $id)
    {
        return (new ExpenseService)
        ->request($request)
        ->update($id);
    }

    public function destroy($id)
    {
        return (new ExpenseService)
            ->delete($id);
    }
}
