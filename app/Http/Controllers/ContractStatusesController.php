<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\ContractStatusesService;


class ContractStatusesController extends Controller
{
    public function index()
    {
        return (new ContractStatusesService)->all();    
    }

    public function store(Request $request)
    {
        return (new ContractStatusesService)
            ->request($request)
            ->create();
    }

    public function show(Request $request, $id)
    {
        return (new ContractStatusesService)
            ->request($request)
            ->one($id);
    }

    public function update(Request $request, $id)
    {
        return (new ContractStatusesService)
        ->request($request)
        ->update($id);
    }

    public function destroy($id)
    {
        return (new ContractStatusesService)
            ->delete($id);
    }
}
