<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\AccessService;

class AccessController extends Controller
{
 
    public function index()
    {
        return (new AccessService)->all();    
    }

    public function store(Request $request)
    {
        return (new AccessService)
        ->request($request)
        ->create();
    }

    public function update(Request $request, $id)
    {
        return (new AccessService)
        ->request($request)
        ->update($id);
    }

}
