<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\FileService;

class FileController extends Controller
{
    public function index()
    {
        return (new FileService)->all();    
    }

    public function store(Request $request)
    {
        return (new FileService)
            ->request($request)
            ->create();
    }

    public function show(Request $request, $id)
    {
        return (new FileService)
            ->request($request)
            ->one($id);
    }

    public function destroy($id)
    {
        return (new FileService)
            ->delete($id);
    }
}
