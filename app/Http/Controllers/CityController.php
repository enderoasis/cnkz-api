<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\CityService;

class CityController extends Controller
{
 
    public function index()
    {
        return (new CityService)->all();    
    }

    public function store(Request $request)
    {
        return (new CityService)
        ->request($request)
        ->create();
    }

    public function destroy($id)
    {
        return (new CityService)
            ->delete($id);
    }
}
