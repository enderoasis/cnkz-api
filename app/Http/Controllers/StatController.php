<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\StatService;

class StatController extends Controller
{
    public function index()
    {
        return (new StatService)->all();    
    }

   
}
