<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\UserService;

class UserController extends Controller
{
    public function index()
    {
        return (new UserService)->all();    
    }

    public function store(Request $request)
    {
        return (new UserService)
            ->request($request)
            ->create(null);
    }

    public function show(Request $request, $id)
    {
        return (new UserService)
            ->request($request)
            ->one($id);
    }

    public function update(Request $request, $id)
    {
        return (new UserService)
        ->request($request)
        ->update($id);
    }

    public function destroy($id)
    {
        return (new UserService)
            ->delete($id);
    }
}
