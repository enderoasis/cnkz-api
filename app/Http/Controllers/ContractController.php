<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\ContractService;


class ContractController extends Controller
{
    public function index()
    {
        return (new ContractService)->all();    
    }

    public function store(Request $request)
    {
        return (new ContractService)
            ->request($request)
            ->create();
    }

    public function show(Request $request, $id)
    {
        return (new ContractService)
            ->request($request)
            ->one($id);
    }

    public function update(Request $request, $id)
    {
        return (new ContractService)
        ->request($request)
        ->update($id);
    }

    public function destroy($id)
    {
        return (new ContractService)
            ->delete($id);
    }
    public function getClientContractsById($id)
    {
        return (new ContractService)
            ->getClientContracts($id);
    }
    public function formExcel()
    {
        return (new ContractService)
        ->getExcel();
    }
}
