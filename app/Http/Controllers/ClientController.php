<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\ClientService;

class ClientController extends Controller
{
    public function index()
    {
        return (new ClientService)->all();    
    }

    public function store(Request $request)
    {
        return (new ClientService)
        ->request($request)
        ->create();
    }

    public function update(Request $request, $id)
    {
        return (new ClientService)
        ->request($request)
        ->update($id);
    }

    public function show(Request $request, $id)
    {
        return (new ClientService)
            ->request($request)
            ->one($id);
    }

    public function destroy($id)
    {
        return (new ClientService)
            ->delete($id);
    }

    public function formExcel()
    {
        return (new ClientService)
        ->getExcel();
    }
}
