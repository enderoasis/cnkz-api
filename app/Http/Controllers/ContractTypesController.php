<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\ContractTypeService;


class ContractTypeController extends Controller
{
    public function index()
    {
        return (new ContractTypeService)->all();    
    }

    public function store(Request $request)
    {
        return (new ContractTypeService)
            ->request($request)
            ->create();
    }

    public function show(Request $request, $id)
    {
        return (new ContractTypeService)
            ->request($request)
            ->one($id);
    }

    public function update(Request $request, $id)
    {
        return (new ContractTypeService)
        ->request($request)
        ->update($id);
    }

    public function destroy($id)
    {
        return (new ContractTypeService)
            ->delete($id);
    }
}
