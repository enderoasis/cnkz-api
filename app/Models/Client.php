<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use  App\Models\Contract;

class Client extends Model
{
    use HasFactory;
    
    protected $fillable = ['company_name','name','surname','birth_date','phone','city_id','email'];
    protected $casts = [
        'created_at'  => 'date:Y-m-d'
    ];

    public function contracts()
    {
        return $this->hasMany(Contract::class);
    }

}
