<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class Contract extends Model
{
    use HasFactory;
    protected $fillable = ['name','type_id','city_id','client_id','status_id','contract_number','quadrature','city_id','start_date','end_date'];
   

    public function getCurrentUserContracts()
    {
            try {
                $user = auth()->userOrFail();
            } catch (\Tymon\JWTAuth\Exceptions\UserNotDefinedException $e) {
                throw new BadRequestHttpException('401');
            }

            if($user->can('contract-view-all'))
            {
                return self::leftJoin('cities', 'cities.id', '=', 'contracts.city_id')
                ->leftJoin('contract_types', 'contract_types.id', '=', 'contracts.type_id')
                ->leftJoin('contract_statuses', 'contract_statuses.id', '=', 'contracts.status_id')
                ->leftJoin('clients', 'clients.id', '=', 'contracts.client_id')
                ->select('contracts.id as id',
                    'contracts.quadrature as quadrature',
                    'contracts.contract_number as contract_number',
                    \DB::raw('CONCAT(clients.surname, " ", clients.name) AS client_name'),
                    'contract_types.name as type_name',
                    'cities.name as city_name',
                    'contract_statuses.name as status_name',
                    'contract_statuses.status_color as status_color',
                    'contracts.start_date as start_date',
                    'contracts.end_date as end_date',
                    'contracts.notify_date as notify_date'
                )
                ->orderby('id','DESC')
                ->get();
            }
    
            if($user->can('contract-view-own'))
            {
                return self::leftJoin('cities', 'cities.id', '=', 'contracts.city_id')
                ->leftJoin('contract_types', 'contract_types.id', '=', 'contracts.type_id')
                ->leftJoin('contract_statuses', 'contract_statuses.id', '=', 'contracts.status_id')
                ->leftJoin('clients', 'clients.id', '=', 'contracts.client_id')
                ->where('clients.email',$user->email)
                ->select('contracts.id as id',
                    'contracts.quadrature as quadrature',
                    'contracts.contract_number as contract_number',
                    \DB::raw('CONCAT(clients.surname, " ", clients.name) AS client_name'),
                    'contract_types.name as type_name',
                    'cities.name as city_name',
                    'contract_statuses.name as status_name',
                    'contract_statuses.status_color as status_color',
                    'contracts.start_date as start_date',
                    'contracts.end_date as end_date',
                    'contracts.notify_date as notify_date'
                )
                ->orderby('id','DESC')
                ->get();
            }
    
            if($user->can('contract-view-city'))
            {
                return self::leftJoin('cities', 'cities.id', '=', 'contracts.city_id')
                ->leftJoin('contract_types', 'contract_types.id', '=', 'contracts.type_id')
                ->leftJoin('contract_statuses', 'contract_statuses.id', '=', 'contracts.status_id')
                ->leftJoin('clients', 'clients.id', '=', 'contracts.client_id')
                ->where('contracts.city_id',$user->city_id)
                ->select('contracts.id as id',
                    'contracts.quadrature as quadrature',
                    'contracts.contract_number as contract_number',
                    \DB::raw('CONCAT(clients.surname, " ", clients.name) AS client_name'),
                    'contract_types.name as type_name',
                    'cities.name as city_name',
                    'contract_statuses.name as status_name',
                    'contract_statuses.status_color as status_color',
                    'contracts.start_date as start_date',
                    'contracts.end_date as end_date',
                    'contracts.notify_date as notify_date'
                )
                ->orderby('id','DESC')
                ->get();
            }   
    }

    public function client()
    {
        return $this->belongsTo(Client::class);
    }
    public function status()
    {
        return $this->hasOne(ContractStatus::class);
    }
    public function incomes()
    {
        return $this->hasMany(Income::class);
    }
    public function expenses()
    {
        return $this->hasMany(Expense::class);
    }
}
