<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Income;
use App\Models\Expense;
use App\Models\Contract;
use App\Models\Client;
use Carbon\Carbon;


class Analytic extends Model
{

    public function getChartsData($id,$city_id)
    {
        $cities = array_map('trim', explode( ",",$city_id ));

            if($id == 1)
            {
                    // Доходы 15%     
                    $income_values = Income::join('contracts', 'contracts.id', '=', 'incomes.contract_id')
                                    ->whereIn('contracts.city_id', $cities)
                                    ->selectRaw('day(incomes.date) as date, sum(incomes.value) * 0.15 as total')
                                    // Внутрений отчет компании - 1
                                    ->where('contracts.type_id', 2)
                                    ->where('incomes.type_id', 1)
                                    ->whereMonth('incomes.date', Carbon::now()->month)
                                    ->orderBy('incomes.date','ASC')
                                    ->groupBy(\DB::raw("day(incomes.date)"))
                                    ->get();

                    // Расходы
                    $expense_values = Expense::join('contracts', 'contracts.id', '=', 'expenses.contract_id')
                                        ->whereIn('contracts.city_id', $cities)
                                        ->selectRaw('day(expenses.date) as date, sum(expenses.value) * 0.15 as total')
                                        ->where('contracts.type_id', 2)
                                        ->where('expenses.type_id', 1)
                                        ->whereMonth('expenses.date', Carbon::now()->month)
                                        ->orderBy('expenses.date','ASC')
                                        ->groupBy(\DB::raw("day(expenses.date)"))
                                        ->get();

                    // Кв.м в управлении и аренде
                    $contracts = Contract::select('quadrature','type_id')->whereIn('city_id',$cities)->whereMonth('created_at', Carbon::now()->month)->get();


                    $income_sum = $income_values->pluck('total')->sum();
                    $expense_sum = $expense_values->pluck('total')->sum();

                    $rented_quadrature_sum = $contracts->where('type_id',1)->pluck('quadrature')->sum();
                    $managed_quadrature_sum = $contracts->where('type_id',2)->pluck('quadrature')->sum();

                    $profit = $income_sum - $expense_sum;

                    $response = [
                        'income_labels' => $income_values->pluck('date'),
                        'income_values' => $income_values->pluck('total'),
                        'expense_labels' => $expense_values->pluck('date'),
                        'expense_values' => $expense_values->pluck('total'),

                        'income_sum' => number_format(round($income_sum,2)),
                        'expense_sum' => number_format(round($expense_sum,2)),
                        'profit' => number_format(round($profit,2)),
                        'rented_quadrature' => $rented_quadrature_sum,
                        'managed_quadrature' => $managed_quadrature_sum
                    ];
            }

            if($id == 2)
            {
                    setlocale(LC_TIME, 'ru_RU');
                    // Доходы 15%
                    $income_values_raw = Income::join('contracts', 'contracts.id', '=', 'incomes.contract_id')
                                            ->whereIn('contracts.city_id', $cities)
                                            ->selectRaw('monthname(incomes.date) as month_name, sum(incomes.value) * 0.15 as total')
                                            ->whereYear('incomes.date', date('Y'))
                                            ->where('contracts.type_id', 2)
                                            ->where('incomes.type_id', 1)
                                            ->orderBy('incomes.date','ASC')
                                            ->groupBy(\DB::raw("monthname(incomes.date)"))
                                            ->get();

                    $expense_values_raw = Expense::join('contracts', 'contracts.id', '=', 'expenses.contract_id')
                                            ->whereIn('contracts.city_id', $cities)
                                            ->selectRaw('monthname(expenses.date) as month_name, sum(expenses.value) as total')
                                            ->whereYear('expenses.date', date('Y'))
                                            ->where('contracts.type_id', 2)
                                            ->where('expenses.type_id', 1)
                                            ->orderBy('expenses.date','ASC')
                                            ->groupBy(\DB::raw("monthname(expenses.date)"))
                                            ->get();
                                    
                    // Кв.м в управлении и аренде
                    $contracts = Contract::select('quadrature','type_id')->whereIn('city_id',$cities)->whereYear('created_at', date('Y'))->get();
   
                    $income_sum = $income_values_raw->pluck('total')->sum();
                    $expense_sum = $expense_values_raw->pluck('total')->sum();
                    $rented_quadrature_sum = $contracts->where('type_id',1)->pluck('quadrature')->sum();
                    $managed_quadrature_sum = $contracts->where('type_id',2)->pluck('quadrature')->sum();
                    $profit = $income_sum - $expense_sum;

                    $response = [
                        'income_labels' => $income_values_raw->pluck('month_name'),
                        'income_values' => $income_values_raw->pluck('total'),
                        'expense_labels' => $expense_values_raw->pluck('month_name'),
                        'expense_values' => $expense_values_raw->pluck('total'),

                        'income_sum' => number_format(round($income_sum,2)),
                        'expense_sum' => number_format(round($expense_sum,2)),
                        'profit' => number_format(round($profit,2)),
                        'rented_quadrature' => $rented_quadrature_sum,
                        'managed_quadrature' => $managed_quadrature_sum
                    ];
            }
            // I
            if($id == 3.1)
            {
                $startOfQ1 = Carbon::now()->month(1)->startOfQuarter();
                $endOfQ1 = Carbon::now()->month(1)->endOfQuarter();

                $income_values_raw = Income::join('contracts', 'contracts.id', '=', 'incomes.contract_id')
                                        ->whereIn('contracts.city_id', $cities)
                                        ->selectRaw('monthname(incomes.date) as month_name, sum(incomes.value) * 0.15 as total')
                                        ->whereBetween('incomes.date', [$startOfQ1, $endOfQ1])
                                        ->where('contracts.type_id', 2)
                                        ->where('incomes.type_id', 1)
                                        ->orderBy('incomes.date','ASC')
                                        ->groupBy(\DB::raw("monthname(incomes.date)"))
                                        ->get();

                    $expense_values_raw = Expense::join('contracts', 'contracts.id', '=', 'expenses.contract_id')
                                            ->whereIn('contracts.city_id', $cities)
                                            ->selectRaw('monthname(expenses.date) as month_name, sum(expenses.value) as total')
                                            ->where('contracts.type_id', 2)
                                            ->where('expenses.type_id', 1)
                                            ->whereBetween('expenses.date', [$startOfQ1, $endOfQ1])
                                            ->orderBy('expenses.date','ASC')
                                            ->groupBy(\DB::raw("monthname(expenses.date)"))
                                            ->get();
                   
                    // Кв.м в управлении и аренде
                    $contracts = Contract::select('quadrature','type_id')->whereIn('city_id',$cities)->whereBetween('created_at', [$startOfQ1, $endOfQ1])->get();
   
                    $income_sum = $income_values_raw->pluck('total')->sum();
                    $expense_sum = $expense_values_raw->pluck('total')->sum();
                    $rented_quadrature_sum = $contracts->where('type_id',1)->pluck('quadrature')->sum();
                    $managed_quadrature_sum = $contracts->where('type_id',2)->pluck('quadrature')->sum();
                    $profit = $income_sum - $expense_sum;

                    $response = [
                        'income_labels' => $income_values_raw->pluck('month_name'),
                        'income_values' => $income_values_raw->pluck('total'),
                        'expense_labels' => $expense_values_raw->pluck('month_name'),
                        'expense_values' => $expense_values_raw->pluck('total'),

                        'income_sum' => number_format(round($income_sum,2)),
                        'expense_sum' => number_format(round($expense_sum,2)),
                        'profit' => number_format(round($profit,2)),
                        'rented_quadrature' => $rented_quadrature_sum,
                        'managed_quadrature' => $managed_quadrature_sum
                    ];
            }
            // II
            if($id == 3.2)
            {
                $startOfQ2 = Carbon::now()->month(4)->startOfQuarter();
                $endOfQ2 = Carbon::now()->month(4)->endOfQuarter();

                $income_values_raw = Income::join('contracts', 'contracts.id', '=', 'incomes.contract_id')
                                        ->whereIn('contracts.city_id', $cities)
                                        ->selectRaw('monthname(incomes.date) as month_name, sum(incomes.value) * 0.15 as total')
                                        ->whereBetween('incomes.created_at', [$startOfQ2, $endOfQ2])
                                        ->where('contracts.type_id', 2)
                                        ->where('incomes.type_id', 1)
                                        ->orderBy('incomes.date','ASC')
                                        ->groupBy(\DB::raw("monthname(incomes.date)"))
                                        ->get();

                $expense_values_raw = Expense::join('contracts', 'contracts.id', '=', 'expenses.contract_id')
                                        ->whereIn('contracts.city_id', $cities)
                                        ->selectRaw('monthname(expenses.date) as month_name, sum(expenses.value) as total')
                                        ->where('contracts.type_id', 2)
                                        ->where('expenses.type_id', 1)
                                        ->whereBetween('expenses.date', [$startOfQ2, $endOfQ2])
                                        ->orderBy('expenses.date','ASC')
                                        ->groupBy(\DB::raw("monthname(expenses.date)"))
                                        ->get();
            
                // Кв.м в управлении и аренде
                $contracts = Contract::select('quadrature','type_id')->whereIn('city_id',$cities)->whereBetween('created_at', [$startOfQ2, $endOfQ2])->get();
   
                $income_sum = $income_values_raw->pluck('total')->sum();
                $expense_sum = $expense_values_raw->pluck('total')->sum();
                $rented_quadrature_sum = $contracts->where('type_id',1)->pluck('quadrature')->sum();
                $managed_quadrature_sum = $contracts->where('type_id',2)->pluck('quadrature')->sum();
                $profit = $income_sum - $expense_sum;

                $response = [
                    'income_labels' => $income_values_raw->pluck('month_name'),
                    'income_values' => $income_values_raw->pluck('total'),
                    'expense_labels' => $expense_values_raw->pluck('month_name'),
                    'expense_values' => $expense_values_raw->pluck('total'),

                    'income_sum' => number_format(round($income_sum,2)),
                    'expense_sum' => number_format(round($expense_sum,2)),
                    'profit' => number_format(round($profit,2)),
                    'rented_quadrature' => $rented_quadrature_sum,
                    'managed_quadrature' => $managed_quadrature_sum
                ];
            }
            // III
            if($id == 3.3)
            {
                $startOfQ3 = Carbon::now()->month(7)->startOfQuarter();
                $endOfQ3 = Carbon::now()->month(7)->endOfQuarter();

                $income_values_raw = Income::join('contracts', 'contracts.id', '=', 'incomes.contract_id')
                                        ->whereIn('contracts.city_id', $cities)
                                        ->selectRaw('monthname(incomes.date) as month_name, sum(incomes.value) * 0.15 as total')
                                        ->whereBetween('incomes.date', [$startOfQ3, $endOfQ3])
                                        ->where('contracts.type_id', 2)
                                        ->where('incomes.type_id', 1)
                                        ->orderBy('incomes.date','ASC')
                                        ->groupBy(\DB::raw("monthname(incomes.date)"))
                ->get();

                $expense_values_raw = Expense::join('contracts', 'contracts.id', '=', 'expenses.contract_id')
                                        ->whereIn('contracts.city_id', $cities)
                                        ->selectRaw('monthname(expenses.date) as month_name, sum(expenses.value) as total')
                                        ->where('contracts.type_id', 2)
                                        ->where('expenses.type_id', 1)
                                        ->whereBetween('expenses.date', [$startOfQ3, $endOfQ3])
                                        ->orderBy('expenses.date','ASC')
                                        ->groupBy(\DB::raw("monthname(expenses.date)"))
                                        ->get();
                                
                // Кв.м в управлении и аренде
                $contracts = Contract::select('quadrature','type_id')->whereIn('city_id',$cities)->whereBetween('created_at', [$startOfQ3, $endOfQ3])->get();
   
                $income_sum = $income_values_raw->pluck('total')->sum();
                $expense_sum = $expense_values_raw->pluck('total')->sum();
                $rented_quadrature_sum = $contracts->where('type_id',1)->pluck('quadrature')->sum();
                $managed_quadrature_sum = $contracts->where('type_id',2)->pluck('quadrature')->sum();
                $profit = $income_sum - $expense_sum;

                $response = [
                    'income_labels' => $income_values_raw->pluck('month_name'),
                    'income_values' => $income_values_raw->pluck('total'),
                    'expense_labels' => $expense_values_raw->pluck('month_name'),
                    'expense_values' => $expense_values_raw->pluck('total'),

                    'income_sum' => number_format(round($income_sum,2)),
                    'expense_sum' => number_format(round($expense_sum,2)),
                    'profit' => number_format(round($profit,2)),
                    'rented_quadrature' => $rented_quadrature_sum,
                    'managed_quadrature' => $managed_quadrature_sum
                ];
            }
            // IV
            if($id == 3.4)
            {
                $startOfQ4 = Carbon::now()->month(10)->startOfQuarter();
                $endOfQ4 = Carbon::now()->month(10)->endOfQuarter();

                $income_values_raw = Income::join('contracts', 'contracts.id', '=', 'incomes.contract_id')
                                        ->whereIn('contracts.city_id', $cities)
                                        ->selectRaw('monthname(incomes.date) as month_name, sum(incomes.value) * 0.15 as total')
                                        ->whereBetween('incomes.date', [$startOfQ4, $endOfQ4])
                                        ->where('contracts.type_id', 2)
                                        ->where('incomes.type_id', 1)
                                        ->orderBy('incomes.date','ASC')
                                        ->groupBy(\DB::raw("monthname(incomes.date)"))
                                        ->get();

                $expense_values_raw = Expense::join('contracts', 'contracts.id', '=', 'expenses.contract_id')
                                        ->whereIn('contracts.city_id', $cities)
                                        ->selectRaw('monthname(expenses.date) as month_name, sum(expenses.value) as total')
                                        ->where('contracts.type_id', 2)
                                        ->where('expenses.type_id', 1)
                                        ->whereBetween('expenses.date', [$startOfQ4, $endOfQ4])
                                        ->orderBy('expenses.date','ASC')
                                        ->groupBy(\DB::raw("monthname(expenses.date)"))
                                        ->get();
            
                // Кв.м в управлении и аренде
                $contracts = Contract::select('quadrature','type_id')->whereIn('city_id',$cities)->whereBetween('created_at', [$startOfQ4, $endOfQ4])->get();
   
                $income_sum = $income_values_raw->pluck('total')->sum();
                $expense_sum = $expense_values_raw->pluck('total')->sum();
                $rented_quadrature_sum = $contracts->where('type_id',1)->pluck('quadrature')->sum();
                $managed_quadrature_sum = $contracts->where('type_id',2)->pluck('quadrature')->sum();
                $profit = $income_sum - $expense_sum;

                $response = [
                    'income_labels' => $income_values_raw->pluck('month_name'),
                    'income_values' => $income_values_raw->pluck('total'),
                    'expense_labels' => $expense_values_raw->pluck('month_name'),
                    'expense_values' => $expense_values_raw->pluck('total'),

                    'income_sum' => number_format(round($income_sum,2)),
                    'expense_sum' => number_format(round($expense_sum,2)),
                    'profit' => number_format(round($profit,2)),
                    'rented_quadrature' => $rented_quadrature_sum,
                    'managed_quadrature' => $managed_quadrature_sum
                ];
            }

            return $response;
    }

}
