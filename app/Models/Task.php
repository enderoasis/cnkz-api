<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Task extends Model
{
    use HasFactory, Notifiable;

    protected $fillable = ['id','assignee_id','start_date','end_date','title','description'];

    public function status()
    {
        return $this->hasOne(TaskStatus::class,'id');
    }

    public function routeNotificationForTelegram()
    {
        return -683843713;
    }

    public static function getCurrentUserTasks($start,$end)
    {
        try {
            $user = auth()->userOrFail();
        } catch (\Tymon\JWTAuth\Exceptions\UserNotDefinedException $e) {
            //Log::channel('telegram')->error($e->getMessage());
            return response()->json(['error' => 'Unauthorized'], 401);
        }   

        if($user->can('task-view-all'))
        {
            if($start && !$end)
            {
                    $start_date = Carbon::parse(request('start_date'))->format('Y-m-d');
                    $tasks = Task::whereDate('start_date' , '=', $start_date)->get();
            }

            if(!$start && $end)
            {
                    $end_date = Carbon::parse(request('end_date'))->format('Y-m-d');
                    $tasks = Task::whereDate('end_date' , '=', $end_date)->get();
            }

            if($start && $end)
            {
                    $start_date = Carbon::parse(request('start_date'))->format('Y-m-d');
                    $end_date = Carbon::parse(request('end_date'))->format('Y-m-d');
                
                    $tasks = Task::where([['start_date','<=',$start_date],['end_date','>=',$end_date]])
                    ->orwhereBetween('start_date',array($start_date,$end_date))
                    ->orWhereBetween('end_date',array($start_date,$end_date))->get();
            }

            if(!$start && !$end)
            {
                    $tasks = Task::all();
            }
        }
        else if($user->can('task-view-own'))
        {
            if($start && !$end)
            {
                    $start_date = Carbon::parse(request('start_date'))->format('Y-m-d');
                    $tasks = Task::whereDate('start_date' , '=', $start_date)->where('assignee_id',$user->id)->orWhere('owner_id',$user->id)->get();
            }

            if(!$start && $end)
            {
                    $end_date = Carbon::parse(request('end_date'))->format('Y-m-d');
                    $tasks = Task::whereDate('end_date' , '=', $end_date)->where('assignee_id',$user->id)->orWhere('owner_id',$user->id)->get();
            }

            if($start && $end)
            {
                    $start_date = Carbon::parse(request('start_date'))->format('Y-m-d');
                    $end_date = Carbon::parse(request('end_date'))->format('Y-m-d');
                
                    $tasks = Task::where([['start_date','<=',$start_date],['end_date','>=',$end_date]])
                            ->orwhereBetween('start_date',array($start_date,$end_date))
                            ->orWhereBetween('end_date',array($start_date,$end_date))
                            ->where('assignee_id',$user->id)
                            ->orWhere('owner_id',$user->id)
                            ->get();
            }

            if(!$start && !$end)
            {
                    $tasks = Task::where('assignee_id', $user->id)->orWhere('owner_id',$user->id)->get();
            }
        }
              return $tasks;
    }
}
