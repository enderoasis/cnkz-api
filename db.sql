-- MySQL dump 10.13  Distrib 5.7.25, for Win64 (x86_64)
--
-- Host: localhost    Database: cnkz-api
-- ------------------------------------------------------
-- Server version	5.7.25

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `cities`
--

DROP TABLE IF EXISTS `cities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cities` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cities`
--

LOCK TABLES `cities` WRITE;
/*!40000 ALTER TABLE `cities` DISABLE KEYS */;
INSERT INTO `cities` VALUES (1,'Алматы',NULL,NULL),(2,'Астана',NULL,NULL);
/*!40000 ALTER TABLE `cities` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `clients`
--

DROP TABLE IF EXISTS `clients`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `clients` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `company_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `surname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `birth_date` date DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `city_id` bigint(20) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `clients_phone_unique` (`phone`),
  UNIQUE KEY `clients_email_unique` (`email`),
  KEY `clients_city_id_foreign` (`city_id`),
  CONSTRAINT `clients_city_id_foreign` FOREIGN KEY (`city_id`) REFERENCES `cities` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `clients`
--

LOCK TABLES `clients` WRITE;
/*!40000 ALTER TABLE `clients` DISABLE KEYS */;
INSERT INTO `clients` VALUES (1,NULL,'Mercedes','Rohan',NULL,'+1-802-598-1510','johnson.modesto@langosh.com',1,NULL,NULL),(2,NULL,'Rodrick','Kautzer',NULL,'275.286.0830 x38316','edwardo85@greenfelder.net',1,NULL,NULL),(3,NULL,'Edgardo','Bosco',NULL,'443-785-0531 x406','ashly75@simonis.com',1,NULL,NULL),(4,NULL,'Quinten','Torp',NULL,'709-495-5504 x08876','roma05@gmail.com',2,NULL,NULL),(5,NULL,'Reyes','Hackett',NULL,'641-491-9996 x089','renner.marlen@yahoo.com',2,NULL,NULL),(6,NULL,'Terrill','McLaughlin',NULL,'659.332.5511 x5596','bartoletti.dean@yahoo.com',1,NULL,NULL),(7,NULL,'Santos','Goyette',NULL,'682-482-7716 x56795','yundt.savanah@gmail.com',2,NULL,NULL),(8,NULL,'Emory','Johns',NULL,'(634) 650-4339','oma59@jakubowski.com',2,NULL,NULL),(9,NULL,'Ayden','Hettinger',NULL,'495-701-3668 x9741','cartwright.katheryn@yahoo.com',2,NULL,NULL),(10,NULL,'Derrick','Kreiger',NULL,'(225) 747-9260 x87354','rowe.jerrell@bednar.com',2,NULL,NULL),(11,NULL,'Katharina','Wilderman',NULL,'601-896-4890','cleta.koss@green.org',2,NULL,NULL),(12,NULL,'Dion','Mitchell',NULL,'+1-724-203-5073','deron51@gmail.com',2,NULL,NULL),(13,NULL,'Marjolaine','Kassulke',NULL,'(824) 340-7810 x558','metz.axel@gmail.com',2,NULL,NULL),(14,NULL,'Miller','Tillman',NULL,'971-793-9631','parker.mabelle@rippin.org',1,NULL,NULL),(15,NULL,'Tyreek','Schneider',NULL,'405.600.9533','ebert.idell@grant.com',2,NULL,NULL),(16,NULL,'Jennings','Pacocha',NULL,'307.774.3665 x77719','ruthie.pollich@schowalter.com',1,NULL,NULL),(17,NULL,'Dario','Mueller',NULL,'215.428.8693 x113','bergnaum.jakayla@harvey.net',2,NULL,NULL),(18,NULL,'Mabel','Kertzmann',NULL,'238.347.7300 x789','tracey64@gmail.com',1,NULL,NULL),(19,NULL,'Merle','Labadie',NULL,'1-584-749-6235 x20095','uyundt@johnston.net',1,NULL,NULL),(20,NULL,'Gina','Blick',NULL,'792.859.1509 x46134','johns.helga@schinner.net',2,NULL,NULL),(21,NULL,'Evie','Grimes',NULL,'+17436312323','zemlak.claudia@wehner.com',1,NULL,NULL),(22,NULL,'Jack','Ruecker',NULL,'(435) 347-8330 x12154','abdullah41@hayes.com',2,NULL,NULL),(23,NULL,'Keagan','Stroman',NULL,'951-805-3501','ruecker.neva@yahoo.com',2,NULL,NULL),(24,NULL,'Vincent','Hahn',NULL,'1-839-245-2546 x1222','muriel80@hotmail.com',2,NULL,NULL),(25,NULL,'Michaela','Goodwin',NULL,'(770) 831-0834','dorris34@jenkins.com',1,NULL,NULL),(26,NULL,'Clementina','Rolfson',NULL,'937.992.6433 x89249','clementine47@metz.com',1,NULL,NULL),(27,NULL,'Monroe','Schuppe',NULL,'(846) 229-1370 x060','gonzalo.jast@hotmail.com',2,NULL,NULL),(28,NULL,'Rahul','Kreiger',NULL,'+1 (556) 275-9547','filomena43@gaylord.com',2,NULL,NULL),(29,NULL,'Arianna','Bartell',NULL,'+18065142653','claire.thiel@yahoo.com',2,NULL,NULL),(30,NULL,'Joe','Jenkins',NULL,'289-828-0623 x7592','slittle@prosacco.com',1,NULL,NULL);
/*!40000 ALTER TABLE `clients` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contract_statuses`
--

DROP TABLE IF EXISTS `contract_statuses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contract_statuses` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `status_color` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contract_statuses`
--

LOCK TABLES `contract_statuses` WRITE;
/*!40000 ALTER TABLE `contract_statuses` DISABLE KEYS */;
INSERT INTO `contract_statuses` VALUES (1,'На подписание',NULL,NULL,'#fec31c'),(2,'Действующий',NULL,NULL,'#3e9733'),(3,'Закрытый',NULL,NULL,'#cf2837');
/*!40000 ALTER TABLE `contract_statuses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contract_types`
--

DROP TABLE IF EXISTS `contract_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contract_types` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contract_types`
--

LOCK TABLES `contract_types` WRITE;
/*!40000 ALTER TABLE `contract_types` DISABLE KEYS */;
INSERT INTO `contract_types` VALUES (1,'Аренда ',NULL,NULL),(2,'Управление',NULL,NULL);
/*!40000 ALTER TABLE `contract_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contracts`
--

DROP TABLE IF EXISTS `contracts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contracts` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `type_id` bigint(20) unsigned NOT NULL,
  `city_id` bigint(20) unsigned NOT NULL,
  `client_id` bigint(20) unsigned NOT NULL,
  `status_id` bigint(20) unsigned NOT NULL,
  `contract_number` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Номер договора',
  `quadrature` int(11) NOT NULL COMMENT 'Квадратура',
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `notify_date` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `contracts_type_id_foreign` (`type_id`),
  KEY `contracts_city_id_foreign` (`city_id`),
  KEY `contracts_client_id_foreign` (`client_id`),
  KEY `contracts_status_id_foreign` (`status_id`),
  CONSTRAINT `contracts_city_id_foreign` FOREIGN KEY (`city_id`) REFERENCES `cities` (`id`),
  CONSTRAINT `contracts_client_id_foreign` FOREIGN KEY (`client_id`) REFERENCES `clients` (`id`),
  CONSTRAINT `contracts_status_id_foreign` FOREIGN KEY (`status_id`) REFERENCES `contract_statuses` (`id`),
  CONSTRAINT `contracts_type_id_foreign` FOREIGN KEY (`type_id`) REFERENCES `contract_types` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=61 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contracts`
--

LOCK TABLES `contracts` WRITE;
/*!40000 ALTER TABLE `contracts` DISABLE KEYS */;
INSERT INTO `contracts` VALUES (1,2,1,22,2,'8081',193,'2021-12-16','2022-07-12',NULL,NULL,NULL),(2,2,2,2,3,'3541',51,'2021-06-28','2021-10-02',NULL,NULL,NULL),(3,2,1,10,2,'1439',70,'2021-10-07','2021-12-20',NULL,NULL,NULL),(4,1,2,25,3,'6373',140,'2021-05-28','2021-07-02',NULL,NULL,NULL),(5,1,1,3,3,'8630',159,'2022-02-02','2022-03-03',NULL,NULL,NULL),(6,2,1,16,2,'18788',63,'2021-09-28','2022-04-16',NULL,NULL,NULL),(7,2,2,6,1,'1694',231,'2022-01-25','2022-01-09',NULL,NULL,NULL),(8,1,2,20,2,'6596',204,'2022-02-06','2022-10-29',NULL,NULL,NULL),(9,2,2,10,3,'16211',133,'2021-09-23','2023-01-14',NULL,NULL,NULL),(10,2,2,3,1,'13560',109,'2022-02-04','2021-05-03',NULL,NULL,NULL),(11,2,1,23,3,'18270',46,'2021-09-23','2022-08-07',NULL,NULL,NULL),(12,1,2,12,1,'1514',104,'2021-11-07','2021-03-25',NULL,NULL,NULL),(13,2,2,4,1,'3039',297,'2022-01-27','2021-06-08',NULL,NULL,NULL),(14,2,1,7,2,'19983',290,'2021-04-04','2023-02-23',NULL,NULL,NULL),(15,1,1,5,2,'5188',132,'2022-02-21','2022-12-28',NULL,NULL,NULL),(16,1,1,9,1,'14090',61,'2021-08-23','2022-02-17',NULL,NULL,NULL),(17,2,1,19,2,'15097',247,'2021-03-11','2021-04-17',NULL,NULL,NULL),(18,2,1,12,2,'7855',96,'2021-05-08','2021-04-30',NULL,NULL,NULL),(19,1,1,27,3,'17376',276,'2022-02-07','2023-01-26',NULL,NULL,NULL),(20,1,1,21,2,'1979',177,'2022-01-09','2022-11-07',NULL,NULL,NULL),(21,2,1,6,2,'10897',173,'2022-01-07','2021-09-21',NULL,NULL,NULL),(22,2,1,19,2,'1862',38,'2021-05-18','2021-11-10',NULL,NULL,NULL),(23,2,1,11,2,'15036',183,'2021-10-01','2022-05-05',NULL,NULL,NULL),(24,2,1,19,3,'5554',150,'2021-12-25','2023-02-19',NULL,NULL,NULL),(25,2,1,15,2,'2725',77,'2022-02-13','2022-02-08',NULL,NULL,NULL),(26,2,2,29,3,'12467',158,'2022-01-13','2021-08-18',NULL,NULL,NULL),(27,2,2,24,3,'2926',48,'2021-04-25','2022-03-26',NULL,NULL,NULL),(28,2,2,30,1,'6387',275,'2021-05-28','2022-11-12',NULL,NULL,NULL),(29,1,2,7,2,'13729',235,'2021-05-12','2022-05-19',NULL,NULL,NULL),(30,2,2,16,2,'17595',236,'2022-01-27','2021-03-31',NULL,NULL,NULL),(31,1,2,24,1,'6676',293,'2022-02-23','2022-01-30',NULL,NULL,NULL),(32,1,2,29,3,'14200',50,'2021-12-13','2022-10-26',NULL,NULL,NULL),(33,1,2,26,2,'5755',242,'2021-05-31','2022-03-06',NULL,NULL,NULL),(34,2,1,29,1,'9284',126,'2021-10-30','2022-11-20',NULL,NULL,NULL),(35,2,2,14,1,'2475',91,'2021-07-02','2022-06-01',NULL,NULL,NULL),(36,1,2,3,3,'11527',42,'2021-12-02','2022-07-07',NULL,NULL,NULL),(37,1,1,19,3,'16920',262,'2021-11-12','2021-03-26',NULL,NULL,NULL),(38,1,1,15,3,'7279',295,'2021-12-21','2022-01-03',NULL,NULL,NULL),(39,2,2,19,1,'13286',182,'2021-11-17','2023-02-22',NULL,NULL,NULL),(40,2,1,24,2,'16024',108,'2021-10-03','2022-06-06',NULL,NULL,NULL),(41,2,1,7,1,'5462',201,'2021-04-22','2021-06-11',NULL,NULL,NULL),(42,2,1,13,2,'11178',113,'2021-04-11','2021-04-15',NULL,NULL,NULL),(43,1,1,7,1,'19221',111,'2021-09-30','2022-03-14',NULL,NULL,NULL),(44,1,1,21,3,'14480',203,'2021-04-17','2022-12-24',NULL,NULL,NULL),(45,2,1,5,2,'13338',227,'2021-03-30','2022-03-22',NULL,NULL,NULL),(46,2,2,22,2,'12807',94,'2021-07-07','2021-08-26',NULL,NULL,NULL),(47,2,2,26,2,'19953',252,'2021-09-28','2021-07-16',NULL,NULL,NULL),(48,1,2,11,1,'13461',228,'2022-02-20','2021-12-14',NULL,NULL,NULL),(49,1,2,2,3,'19811',255,'2021-10-24','2022-08-02',NULL,NULL,NULL),(50,1,1,26,2,'14150',220,'2021-08-20','2021-08-30',NULL,NULL,NULL),(51,2,2,14,2,'6563',168,'2022-01-02','2021-09-20',NULL,NULL,NULL),(52,1,1,28,3,'17326',289,'2022-01-21','2021-08-23',NULL,NULL,NULL),(53,1,2,19,3,'18059',49,'2022-02-01','2021-07-18',NULL,NULL,NULL),(54,1,1,16,2,'3609',156,'2021-12-06','2022-09-26',NULL,NULL,NULL),(55,1,2,3,2,'4896',69,'2021-03-19','2022-06-02',NULL,NULL,NULL),(56,2,2,26,3,'12738',128,'2021-11-28','2021-07-04',NULL,NULL,NULL),(57,1,1,13,1,'4352',175,'2021-06-17','2022-12-01',NULL,NULL,NULL),(58,1,1,28,3,'8004',185,'2021-10-22','2022-08-09',NULL,NULL,NULL),(59,2,1,2,3,'4703',242,'2022-01-04','2022-12-30',NULL,NULL,NULL),(60,2,2,25,2,'7266',139,'2021-07-15','2021-12-05',NULL,NULL,NULL);
/*!40000 ALTER TABLE `contracts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `expense_types`
--

DROP TABLE IF EXISTS `expense_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `expense_types` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `expense_types`
--

LOCK TABLES `expense_types` WRITE;
/*!40000 ALTER TABLE `expense_types` DISABLE KEYS */;
INSERT INTO `expense_types` VALUES (1,'Внутренний отчет cn.kz',NULL,NULL),(2,'Отчет клиента',NULL,NULL);
/*!40000 ALTER TABLE `expense_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `expenses`
--

DROP TABLE IF EXISTS `expenses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `expenses` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `type_id` bigint(20) unsigned NOT NULL,
  `contract_id` bigint(20) unsigned NOT NULL,
  `date` date NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `value` int(11) NOT NULL,
  `purpose` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `expenses_type_id_foreign` (`type_id`),
  KEY `expenses_contract_id_foreign` (`contract_id`),
  CONSTRAINT `expenses_contract_id_foreign` FOREIGN KEY (`contract_id`) REFERENCES `contracts` (`id`) ON DELETE CASCADE,
  CONSTRAINT `expenses_type_id_foreign` FOREIGN KEY (`type_id`) REFERENCES `expense_types` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=61 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `expenses`
--

LOCK TABLES `expenses` WRITE;
/*!40000 ALTER TABLE `expenses` DISABLE KEYS */;
INSERT INTO `expenses` VALUES (1,1,13,'2021-09-30',NULL,66318,NULL,NULL,NULL),(2,1,15,'2021-09-20',NULL,68960,NULL,NULL,NULL),(3,1,11,'2021-09-17',NULL,127528,NULL,NULL,NULL),(4,1,5,'2021-11-24',NULL,94854,NULL,NULL,NULL),(5,1,7,'2022-02-22',NULL,167476,NULL,NULL,NULL),(6,1,24,'2021-05-18',NULL,152103,NULL,NULL,NULL),(7,1,22,'2021-04-22',NULL,189680,NULL,NULL,NULL),(8,1,10,'2021-08-05',NULL,66202,NULL,NULL,NULL),(9,1,5,'2021-05-11',NULL,84269,NULL,NULL,NULL),(10,1,19,'2022-02-08',NULL,138637,NULL,NULL,NULL),(11,1,9,'2021-06-14',NULL,110866,NULL,NULL,NULL),(12,1,18,'2021-12-23',NULL,86087,NULL,NULL,NULL),(13,1,21,'2021-07-02',NULL,168274,NULL,NULL,NULL),(14,1,20,'2021-08-18',NULL,121463,NULL,NULL,NULL),(15,1,6,'2021-10-15',NULL,91738,NULL,NULL,NULL),(16,1,5,'2021-09-11',NULL,132337,NULL,NULL,NULL),(17,1,6,'2021-09-03',NULL,80929,NULL,NULL,NULL),(18,1,9,'2021-06-23',NULL,56525,NULL,NULL,NULL),(19,1,11,'2022-02-06',NULL,141411,NULL,NULL,NULL),(20,1,8,'2021-03-01',NULL,54978,NULL,NULL,NULL),(21,1,14,'2021-10-23',NULL,110542,NULL,NULL,NULL),(22,1,9,'2021-08-22',NULL,189183,NULL,NULL,NULL),(23,1,23,'2021-12-26',NULL,104668,NULL,NULL,NULL),(24,1,22,'2022-02-02',NULL,173317,NULL,NULL,NULL),(25,1,12,'2021-03-14',NULL,157807,NULL,NULL,NULL),(26,1,18,'2021-08-17',NULL,77800,NULL,NULL,NULL),(27,1,8,'2021-12-19',NULL,197519,NULL,NULL,NULL),(28,1,7,'2021-10-16',NULL,166519,NULL,NULL,NULL),(29,1,23,'2022-02-07',NULL,128713,NULL,NULL,NULL),(30,1,15,'2021-05-24',NULL,88607,NULL,NULL,NULL),(31,1,12,'2021-06-01',NULL,64109,NULL,NULL,NULL),(32,1,12,'2021-12-29',NULL,94555,NULL,NULL,NULL),(33,1,13,'2021-04-01',NULL,82216,NULL,NULL,NULL),(34,1,6,'2022-01-20',NULL,166021,NULL,NULL,NULL),(35,1,25,'2021-05-26',NULL,104130,NULL,NULL,NULL),(36,1,22,'2022-01-17',NULL,196578,NULL,NULL,NULL),(37,1,2,'2022-02-21',NULL,113874,NULL,NULL,NULL),(38,1,13,'2021-08-26',NULL,86781,NULL,NULL,NULL),(39,1,23,'2021-10-11',NULL,162748,NULL,NULL,NULL),(40,1,23,'2021-10-20',NULL,127416,NULL,NULL,NULL),(41,1,5,'2021-08-10',NULL,113508,NULL,NULL,NULL),(42,1,19,'2021-12-09',NULL,73746,NULL,NULL,NULL),(43,1,1,'2021-09-06',NULL,160102,NULL,NULL,NULL),(44,1,2,'2022-01-17',NULL,107107,NULL,NULL,NULL),(45,1,1,'2021-08-20',NULL,190320,NULL,NULL,NULL),(46,1,6,'2021-11-14',NULL,59924,NULL,NULL,NULL),(47,1,20,'2021-03-13',NULL,59193,NULL,NULL,NULL),(48,1,11,'2021-10-26',NULL,165804,NULL,NULL,NULL),(49,1,18,'2021-04-19',NULL,72995,NULL,NULL,NULL),(50,1,14,'2021-10-24',NULL,195548,NULL,NULL,NULL),(51,1,10,'2021-09-28',NULL,81555,NULL,NULL,NULL),(52,1,19,'2021-10-05',NULL,140730,NULL,NULL,NULL),(53,1,12,'2021-03-08',NULL,119679,NULL,NULL,NULL),(54,1,4,'2021-12-08',NULL,157666,NULL,NULL,NULL),(55,1,14,'2021-10-14',NULL,139531,NULL,NULL,NULL),(56,1,6,'2021-12-30',NULL,122698,NULL,NULL,NULL),(57,1,20,'2022-02-15',NULL,100282,NULL,NULL,NULL),(58,1,25,'2021-07-29',NULL,182121,NULL,NULL,NULL),(59,1,8,'2021-06-08',NULL,113823,NULL,NULL,NULL),(60,1,3,'2021-05-01',NULL,153324,NULL,NULL,NULL);
/*!40000 ALTER TABLE `expenses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `failed_jobs`
--

DROP TABLE IF EXISTS `failed_jobs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `failed_jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `failed_jobs`
--

LOCK TABLES `failed_jobs` WRITE;
/*!40000 ALTER TABLE `failed_jobs` DISABLE KEYS */;
/*!40000 ALTER TABLE `failed_jobs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `files`
--

DROP TABLE IF EXISTS `files`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `files` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL,
  `model_id` bigint(20) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `format` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `model_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `files_user_id_foreign` (`user_id`),
  CONSTRAINT `files_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `files`
--

LOCK TABLES `files` WRITE;
/*!40000 ALTER TABLE `files` DISABLE KEYS */;
/*!40000 ALTER TABLE `files` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `income_types`
--

DROP TABLE IF EXISTS `income_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `income_types` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `income_types`
--

LOCK TABLES `income_types` WRITE;
/*!40000 ALTER TABLE `income_types` DISABLE KEYS */;
INSERT INTO `income_types` VALUES (1,'Внутренний отчет cn.kz',NULL,NULL),(2,'Отчет клиента',NULL,NULL);
/*!40000 ALTER TABLE `income_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `incomes`
--

DROP TABLE IF EXISTS `incomes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `incomes` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `type_id` bigint(20) unsigned NOT NULL,
  `contract_id` bigint(20) unsigned NOT NULL,
  `date` date NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `value` int(11) NOT NULL,
  `purpose` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `incomes_type_id_foreign` (`type_id`),
  KEY `incomes_contract_id_foreign` (`contract_id`),
  CONSTRAINT `incomes_contract_id_foreign` FOREIGN KEY (`contract_id`) REFERENCES `contracts` (`id`) ON DELETE CASCADE,
  CONSTRAINT `incomes_type_id_foreign` FOREIGN KEY (`type_id`) REFERENCES `income_types` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=61 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `incomes`
--

LOCK TABLES `incomes` WRITE;
/*!40000 ALTER TABLE `incomes` DISABLE KEYS */;
INSERT INTO `incomes` VALUES (1,1,9,'2022-01-02',NULL,249190,NULL,NULL,NULL),(2,1,20,'2021-09-18',NULL,311541,NULL,NULL,NULL),(3,1,14,'2021-05-03',NULL,365703,NULL,NULL,NULL),(4,1,21,'2021-03-15',NULL,367637,NULL,NULL,NULL),(5,1,3,'2021-08-31',NULL,495747,NULL,NULL,NULL),(6,1,5,'2022-01-21',NULL,381960,NULL,NULL,NULL),(7,1,16,'2021-12-13',NULL,274390,NULL,NULL,NULL),(8,1,14,'2022-02-27',NULL,497453,NULL,NULL,NULL),(9,1,15,'2021-07-18',NULL,463073,NULL,NULL,NULL),(10,1,23,'2021-04-02',NULL,383572,NULL,NULL,NULL),(11,1,5,'2021-03-04',NULL,353131,NULL,NULL,NULL),(12,1,22,'2021-06-18',NULL,367097,NULL,NULL,NULL),(13,1,6,'2021-10-08',NULL,320973,NULL,NULL,NULL),(14,1,8,'2021-06-15',NULL,418795,NULL,NULL,NULL),(15,1,11,'2021-03-05',NULL,223258,NULL,NULL,NULL),(16,1,18,'2021-12-10',NULL,302601,NULL,NULL,NULL),(17,1,18,'2021-12-09',NULL,444865,NULL,NULL,NULL),(18,1,5,'2021-08-25',NULL,344098,NULL,NULL,NULL),(19,1,24,'2021-12-25',NULL,467146,NULL,NULL,NULL),(20,1,11,'2021-12-03',NULL,359314,NULL,NULL,NULL),(21,1,15,'2021-09-24',NULL,494788,NULL,NULL,NULL),(22,1,12,'2021-03-29',NULL,328013,NULL,NULL,NULL),(23,1,17,'2022-02-24',NULL,283133,NULL,NULL,NULL),(24,1,21,'2021-04-04',NULL,212978,NULL,NULL,NULL),(25,1,4,'2021-04-30',NULL,370338,NULL,NULL,NULL),(26,1,4,'2021-04-05',NULL,433835,NULL,NULL,NULL),(27,1,4,'2021-11-06',NULL,382760,NULL,NULL,NULL),(28,1,24,'2021-03-17',NULL,463628,NULL,NULL,NULL),(29,1,23,'2021-06-12',NULL,425183,NULL,NULL,NULL),(30,1,20,'2021-09-21',NULL,400136,NULL,NULL,NULL),(31,1,17,'2021-09-01',NULL,244999,NULL,NULL,NULL),(32,1,12,'2021-03-14',NULL,452040,NULL,NULL,NULL),(33,1,11,'2021-07-29',NULL,461661,NULL,NULL,NULL),(34,1,12,'2021-07-29',NULL,331089,NULL,NULL,NULL),(35,1,25,'2021-07-22',NULL,454288,NULL,NULL,NULL),(36,1,11,'2021-05-18',NULL,229403,NULL,NULL,NULL),(37,1,25,'2022-01-15',NULL,473851,NULL,NULL,NULL),(38,1,13,'2021-03-23',NULL,353121,NULL,NULL,NULL),(39,1,17,'2021-08-13',NULL,208894,NULL,NULL,NULL),(40,1,18,'2022-02-09',NULL,232735,NULL,NULL,NULL),(41,1,10,'2021-09-23',NULL,434030,NULL,NULL,NULL),(42,1,10,'2021-11-27',NULL,344769,NULL,NULL,NULL),(43,1,12,'2021-07-07',NULL,455132,NULL,NULL,NULL),(44,1,18,'2022-01-31',NULL,299830,NULL,NULL,NULL),(45,1,24,'2021-11-12',NULL,277367,NULL,NULL,NULL),(46,1,14,'2021-05-12',NULL,374661,NULL,NULL,NULL),(47,1,6,'2021-08-07',NULL,342058,NULL,NULL,NULL),(48,1,13,'2021-04-26',NULL,381450,NULL,NULL,NULL),(49,1,21,'2021-06-22',NULL,232432,NULL,NULL,NULL),(50,1,13,'2021-06-30',NULL,273325,NULL,NULL,NULL),(51,1,19,'2021-09-05',NULL,444223,NULL,NULL,NULL),(52,1,8,'2021-06-23',NULL,310079,NULL,NULL,NULL),(53,1,2,'2021-04-26',NULL,326862,NULL,NULL,NULL),(54,1,15,'2021-06-09',NULL,365528,NULL,NULL,NULL),(55,1,24,'2021-10-13',NULL,451963,NULL,NULL,NULL),(56,1,16,'2021-11-17',NULL,455827,NULL,NULL,NULL),(57,1,8,'2021-04-14',NULL,418545,NULL,NULL,NULL),(58,1,6,'2021-11-27',NULL,445620,NULL,NULL,NULL),(59,1,12,'2021-05-04',NULL,490679,NULL,NULL,NULL),(60,1,10,'2021-12-28',NULL,288940,NULL,NULL,NULL);
/*!40000 ALTER TABLE `incomes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (1,'2014_10_12_000000_create_users_table',1),(2,'2014_10_12_100000_create_password_resets_table',1),(3,'2019_08_19_000000_create_failed_jobs_table',1),(4,'2021_11_14_061440_create_contract_types_table',1),(5,'2021_11_14_061441_create_cities_table',1),(6,'2021_11_14_061442_create_contract_statuses_table',1),(7,'2021_11_14_061443_create_task_statuses_table',1),(8,'2021_11_14_061444_create_income_types_table',1),(9,'2021_11_14_061445_create_expense_types_table',1),(10,'2021_11_14_061446_create_clients_table',1),(11,'2021_11_14_061449_create_contracts_table',1),(12,'2021_11_14_064059_create_tasks_table',1),(13,'2021_11_14_064337_create_incomes_table',1),(14,'2021_11_14_064354_create_expenses_table',1),(15,'2021_11_14_065745_create_permissions_table',1),(16,'2021_11_14_065753_create_roles_table',1),(17,'2021_11_14_065853_create_users_permissions_table',1),(18,'2021_11_14_065931_create_users_roles_table',1),(19,'2021_11_14_070026_create_roles_permissions_table',1),(20,'2021_11_17_140405_add_column_to_users_table',1),(21,'2021_12_06_105652_create_files_table',1),(22,'2022_01_13_101846_add_column_to_contract_statuses_table',1),(23,'2022_01_19_033340_add_column_to_task_statuses_table',1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permissions`
--

DROP TABLE IF EXISTS `permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permissions` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permissions`
--

LOCK TABLES `permissions` WRITE;
/*!40000 ALTER TABLE `permissions` DISABLE KEYS */;
INSERT INTO `permissions` VALUES (1,'Просмотр всех договоров','contract-view-all',NULL,NULL),(2,'Просмотр только своего договора','contract-view-own',NULL,NULL),(3,'Просмотр договоров по городу','contract-view-city',NULL,NULL),(4,'Изменение договоров','contract-edit',NULL,NULL),(5,'Просмотр всех клиентов','client-view-all',NULL,NULL),(6,'Просмотр клиентов по городу','client-view-city',NULL,NULL),(7,'Изменение клиентов','client-edit',NULL,NULL),(8,'Просмотр всех задач','task-view-all',NULL,NULL),(9,'Просмотр своих задач','task-view-own',NULL,NULL),(10,'Изменение всех задач','task-edit-all',NULL,NULL),(11,'Изменение своих задач','task-edit-own',NULL,NULL),(12,'Удаление договоров','contract-delete',NULL,NULL),(13,'Удаление клиента','client-delete',NULL,NULL),(14,'Удаление задач','task-delete',NULL,NULL),(15,'Просмотр доходов','income-view',NULL,NULL),(16,'Редактирование доходов','income-edit',NULL,NULL),(17,'Удаление доходов','income-delete',NULL,NULL),(18,'Просмотр расходов','expense-view',NULL,NULL),(19,'Изменение расходов','expense-edit',NULL,NULL),(20,'Удаление расходов','expense-delete',NULL,NULL),(21,'Просмотр финансового отчёта','report-view',NULL,NULL),(22,'Просмотр аналитики','analytic-view',NULL,NULL),(23,'Редактирование пользователей','user-edit',NULL,NULL),(24,'Просмотр пользователей','user-view',NULL,NULL),(25,'Удаление пользователей','user-delete',NULL,NULL);
/*!40000 ALTER TABLE `permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roles` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` VALUES (1,'Администратор','admin',NULL,NULL),(2,'Менеджер','manager',NULL,NULL),(3,'Клиент','client',NULL,NULL),(4,'Сотрудник','staff',NULL,NULL);
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles_permissions`
--

DROP TABLE IF EXISTS `roles_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roles_permissions` (
  `role_id` bigint(20) unsigned NOT NULL,
  `permission_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`role_id`,`permission_id`),
  KEY `roles_permissions_permission_id_foreign` (`permission_id`),
  CONSTRAINT `roles_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`),
  CONSTRAINT `roles_permissions_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles_permissions`
--

LOCK TABLES `roles_permissions` WRITE;
/*!40000 ALTER TABLE `roles_permissions` DISABLE KEYS */;
INSERT INTO `roles_permissions` VALUES (1,1),(1,2),(3,2),(1,3),(2,3),(1,4),(2,4),(1,5),(1,6),(2,6),(1,7),(2,7),(1,8),(2,8),(1,9),(3,9),(4,9),(1,10),(2,10),(1,11),(3,11),(4,11),(1,12),(1,13),(1,14),(1,15),(2,15),(1,16),(2,16),(1,17),(1,18),(2,18),(1,19),(2,19),(1,20),(1,21),(2,21),(3,21),(1,22),(2,22),(1,23),(1,24),(1,25);
/*!40000 ALTER TABLE `roles_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `task_statuses`
--

DROP TABLE IF EXISTS `task_statuses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `task_statuses` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `status_color` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `task_statuses`
--

LOCK TABLES `task_statuses` WRITE;
/*!40000 ALTER TABLE `task_statuses` DISABLE KEYS */;
INSERT INTO `task_statuses` VALUES (1,'Новая',NULL,NULL,'#0275d8'),(2,'В работе',NULL,NULL,'#fec31c'),(3,'Отмененная',NULL,NULL,'#cf2837'),(4,'Выполненная',NULL,NULL,'#5cb85c');
/*!40000 ALTER TABLE `task_statuses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tasks`
--

DROP TABLE IF EXISTS `tasks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tasks` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `status_id` bigint(20) unsigned NOT NULL DEFAULT '1',
  `assignee_id` int(11) DEFAULT NULL,
  `owner_id` int(11) NOT NULL,
  `start_date` datetime NOT NULL,
  `end_date` datetime NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `tasks_status_id_foreign` (`status_id`),
  CONSTRAINT `tasks_status_id_foreign` FOREIGN KEY (`status_id`) REFERENCES `task_statuses` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tasks`
--

LOCK TABLES `tasks` WRITE;
/*!40000 ALTER TABLE `tasks` DISABLE KEYS */;
/*!40000 ALTER TABLE `tasks` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `surname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `city_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`),
  KEY `users_city_id_foreign` (`city_id`),
  CONSTRAINT `users_city_id_foreign` FOREIGN KEY (`city_id`) REFERENCES `cities` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'Admin','CNKZ','admin@cn.kz',NULL,'$2y$10$.cL3erzh4Svnw6gBDv5.E.T4a/oActJI2kDnMMZo/vd/BAObnHi0m',NULL,'2022-02-28 06:41:05','2022-02-28 06:41:05',1),(2,'Дидар','Темирханов','enderoasis@gmail.com',NULL,'$2y$10$7CweSAw0Klqas8ISV2ph6eQbFkWy5KqoFWAVOlPOmRILR4YlxJAvy',NULL,'2022-02-28 06:41:05','2022-02-28 06:41:05',2);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users_permissions`
--

DROP TABLE IF EXISTS `users_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users_permissions` (
  `user_id` bigint(20) unsigned NOT NULL,
  `permission_id` bigint(20) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`user_id`,`permission_id`),
  KEY `users_permissions_permission_id_foreign` (`permission_id`),
  CONSTRAINT `users_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`),
  CONSTRAINT `users_permissions_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users_permissions`
--

LOCK TABLES `users_permissions` WRITE;
/*!40000 ALTER TABLE `users_permissions` DISABLE KEYS */;
INSERT INTO `users_permissions` VALUES (1,1,NULL,NULL),(1,2,NULL,NULL),(1,3,NULL,NULL),(1,4,NULL,NULL),(1,5,NULL,NULL),(1,6,NULL,NULL),(1,7,NULL,NULL),(1,8,NULL,NULL),(1,9,NULL,NULL),(1,10,NULL,NULL),(1,11,NULL,NULL),(1,12,NULL,NULL),(1,13,NULL,NULL),(1,14,NULL,NULL),(1,15,NULL,NULL),(1,16,NULL,NULL),(1,17,NULL,NULL),(1,18,NULL,NULL),(1,19,NULL,NULL),(1,20,NULL,NULL),(1,21,NULL,NULL),(1,22,NULL,NULL),(1,23,NULL,NULL),(1,24,NULL,NULL),(1,25,NULL,NULL);
/*!40000 ALTER TABLE `users_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users_roles`
--

DROP TABLE IF EXISTS `users_roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users_roles` (
  `user_id` bigint(20) unsigned NOT NULL,
  `role_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`user_id`,`role_id`),
  KEY `users_roles_role_id_foreign` (`role_id`),
  CONSTRAINT `users_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`),
  CONSTRAINT `users_roles_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users_roles`
--

LOCK TABLES `users_roles` WRITE;
/*!40000 ALTER TABLE `users_roles` DISABLE KEYS */;
INSERT INTO `users_roles` VALUES (1,1),(2,2);
/*!40000 ALTER TABLE `users_roles` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-02-28 18:43:48
