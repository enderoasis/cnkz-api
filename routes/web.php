<?php

use Illuminate\Support\Facades\Route;
use App\Mail\UserRegistered;
use App\Models\User;
use App\Models\task;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/mail', function () {
//     \Mail::to('enderoasis@gmail.com')->send(new UserRegistered(User::find(1),'123asd'));
// });

// Route::get('/mail-template', function () {
//     return view('mail.user-registered', [
//         'user' => User::find(1),
//         'tmp_password' => '123qwe'
//     ]);
// });

// Route::get('/task-template', function () {
//     $task = Task::find(1);

//     return view('task.task-assigned', [
//         'user' => User::find(1),
//         'task' => [
//             'id' => $task['id'],
//             'assignee_id' => $task['assignee_id'],
//             'assignee_name' => User::find($task['assignee_id'])->first()->surname . ' ' . User::find($task['assignee_id'])->first()->name,
//             'start' => $task['start_date'],
//             'status_id' => $task['status_id'],
//             'status_name' => \DB::table('task_statuses')->where('id',$task['status_id'])->first()->name,
//             'status_color' => \DB::table('task_statuses')->where('id',$task['status_id'])->first()->status_color,
//             'end' => $task['end_date'],
//             'owner_id' => $task['owner_id'],
//             'owner_name' => User::find($task['owner_id'])->first()->surname . ' ' . User::find($task['owner_id'])->first()->name,
//             'title' => $task['title'],
//             'description' => $task['description']
//         ]
//     ]);
// });


