<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\AuthController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// /api/auth/login, /api/auth/register and etc
// index // store // show //update

Route::group([
    'middleware' => 'api',
    'prefix' => 'auth',
], function ($router) {
    Route::post('/login', [AuthController::class, 'login']);
    Route::post('/register', [AuthController::class, 'register']);
    Route::post('/logout', [AuthController::class, 'logout']);
    Route::post('/refresh', [AuthController::class, 'refresh']);
    Route::get('/user-profile', [AuthController::class, 'userProfile']);    
});

Route::group([
    'middleware' => 'api',
    'namespace' => 'App\Http\Controllers'

], function ($router) {
    Route::resource('contracts', 'ContractController');
    Route::resource('contract/types', 'ContractTypesController');
    Route::resource('contract/statuses', 'ContractStatusesController');
    Route::get('contracts/client/{id}','ContractController@getClientContractsById');
    Route::resource('cities', 'CityController');
    Route::resource('clients', 'ClientController');
    Route::resource('expenses', 'ExpenseController');
    Route::resource('incomes', 'IncomeController');
    Route::resource('tasks', 'TaskController');
    // Notifies of new tasks
    Route::get('notifies','TaskController@getActualNotifies');
    Route::resource('users', 'UserController');

    // Access , roles,permissions
    Route::resource('accesses','AccessController');
    
    //Get chart data
    Route::get('charts','StatController@index');

    Route::get('finance/report', 'FinanceReportController@index');
    //Excel contracts
    Route::get('excel/contracts','ContractController@formExcel');
    //Excel clients
    Route::get('excel/clients','ClientController@formExcel');
    //Excel financial reports
    Route::get('excel/reports','FinanceReportController@formExcel');

    Route::get('pdf/reports','FinanceReportController@formPdf');

    // upload file
    Route::post('files','FileController@store');
    // dowmload file
    Route::get('files/{id}', 'FileController@show')->where('id', '[0-9]+');
    // delete file
    Route::get('files/delete/{id}', 'FileController@destroy')->where('id', '[0-9]+');
    // get model`s files
    Route::get('model/files', 'FileController@index');
});