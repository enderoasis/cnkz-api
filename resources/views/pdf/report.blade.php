<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <title>Финансовый отчёт</title>
    <style>
        body {font-family: DejaVu Sans;}
        table {border-collapse: collapse; width: 100%; }
        table, th, td {border: 1px solid slategray; margin: 5px;}
        .signature { display: inline; width: 30% }
    </style>
</head>
<body>
   
<table>
    <tr>
        <th>Дата</th>
        <th>Сумма</th>
        <th>Тип</th>
        <th>Назначение</th>
        <th>Коммисия</th>
    </tr>
    @foreach($reports as $report)
    <tr>
        <td>{{$report['date']}}</td>
        <td>{{$report['value']}}</td>
        <td>{{$report['type_name']}}</td>
        <td>{{$report['purpose']}}</td>
        <td>{{$report['comission']}}</td>
    </tr>
    @endforeach
</table>
</body>
</html>