<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Permission;
use App\Models\Role;
use App\Models\RolePermission;
use App\Models\User;

class PermissionRoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         /*
         * Назначить все права администратору
         */
        $permissions = Permission::all();

 
        foreach($permissions as $permission)
        {
            \DB::table('roles_permissions')->insert([
                'role_id' => 1,
                'permission_id' => $permission['id']
            ]);

            \DB::table('users_permissions')->insert([
                'user_id' => 1,
                'permission_id' => $permission['id']
            ]);
        }
        /*
         * Доступы к чтению,записи,редактированию к объектам системы
         */
        \DB::table('roles_permissions')->insert([
            [
                'role_id'           => 2,
                'permission_id'     => 3
            ], 
            [
                'role_id'           => 2,
                'permission_id'     => 4
            ], 
            [
                'role_id'           => 2,
                'permission_id'     => 6
            ], 
            [
                'role_id'           => 2,
                'permission_id'     => 7
            ], 
            [
                'role_id'           => 2,
                'permission_id'     => 8
            ], 
            [
                'role_id'           => 2,
                'permission_id'     => 10
            ], 
            [
                'role_id'           => 2,
                'permission_id'     => 15
            ], 
            [
                'role_id'           => 2,
                'permission_id'     => 16
            ], 
            [
                'role_id'           => 2,
                'permission_id'     => 18
            ], 
            [
                'role_id'           => 2,
                'permission_id'     => 19
            ], 
            [
                'role_id'           => 2,
                'permission_id'     => 21
            ],
            [
                'role_id'           => 2,
                'permission_id'     => 22
            ],
            [
                'role_id'           => 3,
                'permission_id'     => 2
            ], 
            [
                'role_id'           => 3,
                'permission_id'     => 9
            ], 
            [
                'role_id'           => 3,
                'permission_id'     => 11
            ], 
            [
                'role_id'           => 3,
                'permission_id'     => 21
            ], 
            [
                'role_id'           => 4,
                'permission_id'     => 9
            ], 
            [
                'role_id'           => 4,
                'permission_id'     => 11
            ]
        ]);
       
    }
}
