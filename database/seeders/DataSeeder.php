<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Income;
use App\Models\Expense;
use App\Models\Contract;
use App\Models\Client;
use Faker\Factory as Faker;

class DataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        
        //Clients fake data
        foreach(range(1,30) as $index){
            \DB::table('clients')->insert([
                'name' => $faker->firstName,
                'surname' => $faker->lastName,
                'phone' => $faker->phoneNumber,
                'email' => $faker->unique()->email,
                'city_id' => rand(1,2)
            ]);
        }
        //Contracts fake data
        foreach(range(1,60) as $index){
            \DB::table('contracts')->insert([
                'type_id' => rand(1,2),
                'city_id' => rand(1,2),
                'client_id' => rand(1,30),
                'status_id' => rand(1,3),
                'contract_number' => rand(1000,20000),
                'quadrature' => rand(30,300),
                'start_date' =>  $faker->dateTimeThisYear(),
                'end_date' => $faker->dateTimeThisYear('+12 months')
            ]);
        }
        //Expense fake data
        foreach(range(1,60) as $index){
            \DB::table('expenses')->insert([
                'type_id' => 1,
                'contract_id' => rand(1,25),
                'value' => rand(50000,200000),
                'date' => $faker->dateTimeThisYear(),
            ]);
        }
        // Incomes fake data
        foreach(range(1,60) as $index){
            \DB::table('incomes')->insert([
                'type_id' => 1,
                'contract_id' => rand(1,25),
                'value' => rand(200000,500000),
                'date' => $faker->dateTimeThisYear(),
            ]);
        }
    }
}
