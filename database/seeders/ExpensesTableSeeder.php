<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class ExpensesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('expense_types')->insert([
            ['name' => 'Внутренний отчет cn.kz'],
            ['name' => 'Отчет клиента']
        ]);
    }
}
