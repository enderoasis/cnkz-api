<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class IncomesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('income_types')->insert([
            ['name' => 'Внутренний отчет cn.kz'],
            ['name' => 'Отчет клиента']
        ]);
    }
}
