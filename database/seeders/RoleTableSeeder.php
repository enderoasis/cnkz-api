<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Role;
use App\Models\User;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Role::insert([
            [
                'name'   => 'Администратор',
                'slug'      => 'admin',
            ], [
                'name'   => 'Менеджер',
                'slug'      => 'manager',
            ], [
                'name'   => 'Клиент',
                'slug'      => 'client',
            ],
            [
                'name' => 'Сотрудник',
                'slug' => 'staff'
            ]
        ]);

        $user = new User;
        $user->name = 'Дидар';
        $user->surname = 'Темирханов';
        $user->email = 'enderoasis@gmail.com';
        $user->password = bcrypt('545123dd');
        $user->city_id = 2;
        $user->save();
        

        \DB::table('users_roles')->insert([
            [
                'user_id' => 1,
                'role_id' => 1
            ],
            [
                'user_id' => 2,
                'role_id' => 2
            ]
        ]);
    }
}
