<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Permission;
use App\Models\User;

class GeneralPermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new User;
        $user->email = 'admin@cn.kz';
        $user->name = 'Admin';
        $user->surname = 'CNKZ';
        $user->password = bcrypt('admin');
        $user->city_id = '1';
        $user->save();

         Permission::insert([
            //1
            [
                'slug' => 'contract-view-all',
                'name' => 'Просмотр всех договоров'
            ],
            //2
            [
                'slug' => 'contract-view-own',
                'name' => 'Просмотр только своего договора'
            ],
            //3
            [
                'slug' => 'contract-view-city',
                'name' => 'Просмотр договоров по городу'
            ],
            //4
            [
                'slug' => 'contract-edit',
                'name' => 'Изменение договоров'
            ],
             //5
             [
                 'slug' => 'client-view-all',
                 'name' => 'Просмотр всех клиентов'
             ],
             //6
             [
                'slug' => 'client-view-city',
                'name' => 'Просмотр клиентов по городу'
             ],
             //7
             [
                 'slug' => 'client-edit',
                 'name' => 'Изменение клиентов'
             ],
             //8
             [
                 'slug' => 'task-view-all',
                 'name' => 'Просмотр всех задач'
             ],
             //9
             [
                'slug' => 'task-view-own',
                'name' => 'Просмотр своих задач'
            ],
             //10
             [
                 'slug' => 'task-edit-all',
                 'name' => 'Изменение всех задач'
             ],
             //11
             [
                'slug' => 'task-edit-own',
                'name' => 'Изменение своих задач'
             ],
             //12
             [
                 'slug' => 'contract-delete',
                 'name' => 'Удаление договоров'
             ],
             //13
             [
                 'slug' => 'client-delete',
                 'name' => 'Удаление клиента'
             ],
             //14
             [
                 'slug' => 'task-delete',
                 'name' => 'Удаление задач'
             ],
             //15
             [
                 'slug' => 'income-view',
                 'name' => 'Просмотр доходов'
             ],
             //16
             [
                 'slug' => 'income-edit',
                 'name' => 'Редактирование доходов'
             ],
             //17
             [
                 'slug' => 'income-delete',
                 'name' => 'Удаление доходов'
             ],
             //18
             [
                 'slug' => 'expense-view',
                 'name' => 'Просмотр расходов'
             ],
             //19
             [
                 'slug' => 'expense-edit',
                 'name' => 'Изменение расходов'
             ],
             //20
             [
                 'slug' => 'expense-delete',
                 'name' => 'Удаление расходов'
             ],
             //21
             [
                 'slug' => 'report-view',
                 'name' => 'Просмотр финансового отчёта'
             ],
             //22
             [
                'slug' => 'analytic-view',
                'name' => 'Просмотр аналитики'
             ],
             //23
             [
                'slug' => 'user-edit',
                'name' => 'Редактирование пользователей'
             ],
             //24
             [
                'slug' => 'user-view',
                'name' => 'Просмотр пользователей'
             ],
             //25
             [
                'slug' => 'user-delete',
                'name' => 'Удаление пользователей'
             ]
         ]);
    }
}
