<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class ContractTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('contract_types')->insert([
            ['name' => 'Аренда '],
            ['name' => 'Управление']
        ]);

        \DB::table('contract_statuses')->insert([
            [
                'name' => 'На подписание',
                'status_color' => '#fec31c'
            ],
            [
                'name' => 'Действующий',
                'status_color' => '#3e9733'
            ],
            [
                'name' => 'Закрытый',
                'status_color' => '#cf2837'
            ]
        ]);
    }
}
