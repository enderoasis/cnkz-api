<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(
            [
                CitiesTableSeeder::class,
                GeneralPermissionSeeder::class,
                RoleTableSeeder::class,
                PermissionRoleTableSeeder::class,
                ContractTableSeeder::class,
                ExpensesTableSeeder::class,
                IncomesTableSeeder::class,
                TasksTableSeeder::class,
            ]
        );
    }
}
