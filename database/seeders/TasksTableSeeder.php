<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class TasksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('task_statuses')->insert([
            [
                'name' => 'Новая',
                'status_color' => '#0275d8'
            ],
            [
                'name' => 'В работе',
                'status_color' => '#fec31c'
            ],
            [
                'name' => 'Отмененная',
                'status_color' => '#cf2837'
            ],
            [
                'name' => 'Выполненная',
                'status_color' => '#5cb85c'    
            ]
        ]);
    }
}
