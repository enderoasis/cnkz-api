<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContractsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contracts', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('type_id');
            $table->unsignedBigInteger('city_id');
            $table->unsignedBigInteger('client_id');
            $table->unsignedBigInteger('status_id');

            $table->foreign('type_id')->references('id')->on('contract_types');
            $table->foreign('city_id')->references('id')->on('cities');
            $table->foreign('client_id')->references('id')->on('clients');
            $table->string('contract_number')->comment('Номер договора');
            $table->integer('quadrature')->comment('Квадратура');
            $table->date('start_date');
            $table->date('end_date');
            $table->date('notify_date')->nullable();
            $table->foreign('status_id')->references('id')->on('contract_statuses');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contracts');
    }
}
